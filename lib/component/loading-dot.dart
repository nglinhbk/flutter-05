import 'package:flutter/material.dart';
import 'package:jumping_dot/jumping_dot.dart';

import 'base-component.dart';

class LoadingDot extends BaseComponentStateful {
  const LoadingDot({
    super.key,
    this.numberOfDots = 5,
    this.color,
  });

  final int numberOfDots;
  final Color? color;

  @override
  State<StatefulWidget> createState() => _LoadingDot();
}

class _LoadingDot extends State<LoadingDot> {
  @override
  Widget build(BuildContext context) {
    print('object');
    return JumpingDots(
      color: widget.color ?? Colors.white,
      radius: 10,
      numberOfDots: widget.numberOfDots,
      verticalOffset: -8,
      animationDuration: const Duration(milliseconds: 200),
    );
  }
}
