import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

import '../common/color.dart';
import 'base-component.dart';
import 'loading-dot.dart';

class ButtonComponent extends BaseComponentStateful {
  const ButtonComponent({
    super.key,
    this.height = 50,
    required this.label,
    this.colorButton,
    this.colorLabel,
    this.fontSizeLabel,
    this.fontWeightLabel,
    this.boxShadows,
    this.onPressed,
    this.loading = false,
  });

  final double height;
  final String label;
  final Color? colorButton;
  final Color? colorLabel;
  final double? fontSizeLabel;
  final FontWeight? fontWeightLabel;
  final List<BoxShadow>? boxShadows;
  final VoidCallback? onPressed;
  final bool loading;

  static Color defaultColorButton = HexColor(color_blue_1);
  static Color defaultColorLabel = Colors.white;
  static double defaultFontSizeLabel = 18;
  static FontWeight defaultFontWeightLabel = FontWeight.w600;

  @override
  State<StatefulWidget> createState() => _ButtonComponent();
}

class _ButtonComponent extends State<ButtonComponent> {
  @override
  Widget build(BuildContext context) {
    List<BoxShadow> defaultBoxShadows = [
      BoxShadow(
        color: (widget.colorButton ?? ButtonComponent.defaultColorButton)
            .withOpacity(0.2),
        spreadRadius: 5,
        blurRadius: 7,
        offset: const Offset(0, 3), // changes position of shadow
      ),
    ];

    return GestureDetector(
      onTap: widget.onPressed,
      child: Container(
        width: double.infinity,
        height: widget.height,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: widget.colorButton ?? ButtonComponent.defaultColorButton,
          borderRadius: BorderRadius.all(Radius.circular(widget.height / 2)),
          boxShadow: widget.boxShadows ?? defaultBoxShadows,
        ),
        child: widget.loading ? const LoadingDot() : button(),
      ),
    );
  }

  Widget button() {
    return Text(
      widget.label,
      style: TextStyle(
        color: widget.colorLabel ?? ButtonComponent.defaultColorLabel,
        fontSize: widget.fontSizeLabel ?? ButtonComponent.defaultFontSizeLabel,
        fontWeight:
            widget.fontWeightLabel ?? ButtonComponent.defaultFontWeightLabel,
      ),
    );
  }
}
