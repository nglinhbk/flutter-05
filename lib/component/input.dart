import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

import '../common/color.dart';
import 'base-component.dart';

class InputComponent extends BaseComponentStateful {
  const InputComponent({
    super.key,
    this.value,
    this.heightInput = 52,
    this.paddingHorizontal = 16,
    this.fontSizeHintStyle = 16,
    this.borderInput,
    this.showBorderInput = true,
    this.focusedBorder,
    this.fillColorInput,
    this.colorTextInput,
    this.inputType,
    this.typePassword = false,
    this.enableInteractiveSelection = true,
    this.obscuringCharacter = '*',
    this.textInputAction,
    this.suffixIcon,
    this.suffixWidget,
    this.prefixIcon,
    this.prefixWidget,
    this.minLines = 1,
    this.maxLines,
    this.autofocus = false,
    this.readOnly = false,
    this.autocorrect = true,
    this.enableSuggestions = true,
    this.textAlign,
    this.labelOverlapDorder = false,
    this.label,
    this.hintText,
    this.labelColor,
    this.labelFontSize,
    this.labelFontWeight,
    this.paddingBottomLabel,
    this.paddingBottomInput,
    this.textCapitalization,
    this.onSubmitted,
    this.onChanged,
    this.errorMessage,
    this.onFocused,
    this.onlyTextField = false,
  });
  final String? value;
  final double heightInput;
  final double paddingHorizontal;
  final double fontSizeHintStyle;
  final InputBorder? borderInput;
  final bool showBorderInput;
  final InputBorder? focusedBorder;
  final Color? fillColorInput;
  final Color? colorTextInput;
  final TextInputType? inputType;
  final bool typePassword;
  final bool enableInteractiveSelection;
  final String obscuringCharacter;
  final TextInputAction? textInputAction;
  final Widget? suffixIcon;
  final Widget? suffixWidget;
  final Widget? prefixIcon;
  final Widget? prefixWidget;
  final int minLines;
  final int? maxLines;
  final bool autofocus;
  final bool readOnly;
  final bool autocorrect;
  final bool enableSuggestions;
  final TextAlign? textAlign;
  final bool labelOverlapDorder;
  final String? label;
  final String? hintText;
  final Color? labelColor;
  final double? labelFontSize;
  final FontWeight? labelFontWeight;
  final double? paddingBottomLabel;
  final double? paddingBottomInput;
  final TextCapitalization? textCapitalization;
  final Function(String)? onSubmitted;
  final Function(String)? onChanged;
  final VoidCallback? onFocused;
  final String? errorMessage;
  final bool onlyTextField;

  // static default variables
  static InputBorder defaultBorder = const OutlineInputBorder(
      borderSide: BorderSide.none,
      borderRadius: BorderRadius.all(Radius.circular(10)));
  static InputBorder defaultFocusedBorder = const OutlineInputBorder(
    borderSide: BorderSide(color: Colors.blue),
    borderRadius: BorderRadius.all(Radius.circular(10)),
  );
  static InputBorder defaultErrorBorder = const OutlineInputBorder(
    borderSide: BorderSide.none,
    borderRadius: BorderRadius.all(Radius.circular(10)),
  );

  static Color defaultFillColorInput = HexColor(color_blue_2);
  static Color defaultFillColorInputError = HexColor(color_error_fill_input);
  static Color defaultColorTextInput = Colors.black54;
  static TextInputType defaultInputType = TextInputType.text;
  static TextInputAction defaultTextInputAction = TextInputAction.done;
  static Icon defaultIconPasswordHiden =
      const Icon(Icons.visibility_off_rounded);
  static Icon defaultIconPasswordShow = const Icon(Icons.visibility_rounded);
  static TextAlign defaultTextAlign = TextAlign.start;
  static Color defaultLabelColor = Colors.black;
  static double defaultLabelFrontSize = 14;
  static FontWeight defaultLabelFontWeight = FontWeight.w500;
  static double defaultPaddingBottomLabel = 7;
  static TextAlign labelAlign = TextAlign.left;
  static double defaultpaddingBottomInput = 25;
  static TextCapitalization defaultTextCapitalization = TextCapitalization.none;
  static double defaultHeightValueNotPassword = 1;
  static double defaultHeightValuePassword = 1.65;
  // end static default variables

  @override
  State<StatefulWidget> createState() => _TextFieldComponent();

  TextStyle getLabelStyle() {
    return TextStyle(
        color: labelColor ?? defaultLabelColor,
        fontSize: labelFontSize ?? defaultLabelFrontSize,
        fontWeight: labelFontWeight ?? defaultLabelFontWeight);
  }
}

class _TextFieldComponent extends State<InputComponent> {
  final FocusNode _focus = FocusNode();
  final TextEditingController controller = TextEditingController();
  bool isFocused = false;
  bool _isShowPassword = false;

  // function
  @override
  void initState() {
    super.initState();

    _focus.addListener(_onFocusChange);

    controller.text = widget.value ?? '';
  }

  @override
  void didUpdateWidget(covariant InputComponent oldWidget) {
    super.didUpdateWidget(oldWidget);
    String value = widget.value ?? '';
    if (controller.text != value) controller.text = widget.value ?? '';
  }

  @override
  void dispose() {
    _focus.removeListener(_onFocusChange);
    _focus.dispose();
    controller.dispose();
    super.dispose();
  }

  void _onFocusChange() {
    isFocused = _focus.hasFocus;
    if (widget.onFocused != null && isFocused) {
      widget.onFocused!.call();
    }
  }

  @override
  Widget build(BuildContext context) {
    // set padding input
    double paddingVerticalInput = widget.heightInput - widget.fontSizeHintStyle;
    if (paddingVerticalInput < 0) paddingVerticalInput = 0;
    paddingVerticalInput = (paddingVerticalInput / 2).toDouble();
    // end padding input

    // set enable Selection
    bool enableSelection =
        widget.typePassword ? false : widget.enableInteractiveSelection;
    // end enable Selection

    Widget iconPassword = IconButton(
      icon: _isShowPassword
          ? InputComponent.defaultIconPasswordHiden
          : InputComponent.defaultIconPasswordShow,
      onPressed: () {
        _isShowPassword = !_isShowPassword;
        setState(() {});
      },
    );

    // set suffix Icon
    Widget? suffixIconWidget =
        widget.typePassword ? iconPassword : widget.suffixIcon;
    // end suffix Icon

    // set max lines
    int? maxLinesInput = widget.typePassword ? 1 : widget.maxLines;
    // end max lines

    // set min lines
    int? minLinesInput = widget.typePassword ? 1 : widget.minLines;
    // end min lines

    // set border input
    InputBorder borderInput = widget.errorMessage == null
        ? (widget.showBorderInput
            ? (widget.borderInput ?? InputComponent.defaultBorder)
            : InputBorder.none)
        : InputComponent.defaultErrorBorder;
    // end border input

    // set fillColor input
    Color fillColor = widget.fillColorInput ??
        (widget.errorMessage != null
            ? InputComponent.defaultFillColorInputError
            : InputComponent.defaultFillColorInput);
    // end fillColor input

    return Stack(
      children: [
        Container(
          padding: widget.onlyTextField
              ? const EdgeInsetsDirectional.all(0)
              : (EdgeInsets.only(
                  bottom: widget.paddingBottomInput ??
                      InputComponent.defaultpaddingBottomInput)),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              widgetLabelOverlapDorder(),
              TextField(
                controller: controller,
                focusNode: _focus,
                onChanged: widget.onChanged,
                onSubmitted: widget.onSubmitted,
                enableInteractiveSelection: enableSelection,
                obscureText: widget.typePassword && !_isShowPassword,
                obscuringCharacter: widget.obscuringCharacter,
                keyboardType:
                    widget.inputType ?? InputComponent.defaultInputType,
                textInputAction: widget.textInputAction ??
                    InputComponent.defaultTextInputAction,
                textAlignVertical: TextAlignVertical.center,
                minLines: minLinesInput,
                maxLines: maxLinesInput,
                autofocus: widget.autofocus,
                readOnly: widget.readOnly,
                autocorrect: widget.autocorrect,
                enableSuggestions: widget.enableSuggestions,
                textAlign: widget.textAlign ?? InputComponent.defaultTextAlign,
                textCapitalization: widget.textCapitalization ??
                    InputComponent.defaultTextCapitalization,
                decoration: InputDecoration(
                    hintText: widget.hintText,
                    labelText: widget.labelOverlapDorder ? widget.label : null,
                    labelStyle: widget.getLabelStyle(),
                    suffixIcon: suffixIconWidget,
                    suffix: widget.suffixWidget,
                    prefix: widget.prefixWidget,
                    prefixIcon: widget.prefixIcon,
                    filled: true,
                    fillColor: fillColor,
                    isCollapsed: true,
                    hintStyle: TextStyle(
                        fontSize: widget.fontSizeHintStyle,
                        color: widget.colorTextInput ??
                            InputComponent.defaultColorTextInput),
                    contentPadding: EdgeInsets.symmetric(
                        vertical: paddingVerticalInput,
                        horizontal: widget.paddingHorizontal),
                    border: borderInput,
                    focusedBorder: widget.showBorderInput
                        ? (widget.focusedBorder ??
                            InputComponent.defaultFocusedBorder)
                        : InputBorder.none,
                    errorBorder: InputComponent.defaultErrorBorder),
              ),
            ],
          ),
        ),
        Positioned(
          top: widget.heightInput + 5,
          child: errorInput(widget.errorMessage),
        ),
      ],
    );
  }

  Widget widgetLabelOverlapDorder() {
    return Visibility(
        visible: !widget.labelOverlapDorder &&
            widget.label != null &&
            !widget.onlyTextField,
        child: Container(
          // width: double.infinity,
          alignment: Alignment.topLeft,
          padding: EdgeInsets.only(
              bottom: widget.paddingBottomLabel ??
                  InputComponent.defaultPaddingBottomLabel),
          child: Text(widget.label ?? '',
              style: widget.getLabelStyle(),
              textAlign: InputComponent.labelAlign),
        ));
  }

  Widget errorInput(String? errorMessage) {
    return Visibility(
      visible: errorMessage != null && widget.onlyTextField,
      child: Text(
        errorMessage ?? '',
        style: TextStyle(color: HexColor(color_error_input), fontSize: 12),
      ),
    );
  }
}
