import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:my_app_flutter/c2p/forgot-password/forgot-password.dart';
import 'package:my_app_flutter/c2p/login.dart';
import 'package:my_app_flutter/common/color.dart';

import '../helper/keyboard.dart';
import 'base-component.dart';

class MainComponent extends BaseComponentStateful {
  const MainComponent({
    super.key,
    required this.body,
    this.title,
    this.dataIconLeft = Icons.arrow_back_ios,
    this.colorIconLeft = Colors.white,
    this.dataIconRights,
    this.backgroundColorAppBar,
    this.elevation,
    this.backgroundColorBody,
    this.centerTitle = false,
    this.hasSingleScrollView = true,
  });

  final Widget body;
  final Text? title;
  final IconData dataIconLeft;
  final List<IconData>? dataIconRights;
  final Color? backgroundColorAppBar;
  final bool centerTitle;
  final double? elevation;
  final Color? backgroundColorBody;
  final Color? colorIconLeft;
  final bool hasSingleScrollView;

  static const Color defaultBackgroundColorBody = Colors.white;

  @override
  State<StatefulWidget> createState() => _MainComponent();
}

class _MainComponent extends State<MainComponent> {
  final scrollController = ScrollController();

  var currentTabIndex = 0;

  @override
  void initState() {
    super.initState();
    scrollController.addListener(onScroll);
  }

  @override
  void dispose() {
    super.dispose();
    scrollController.removeListener(onScroll);
  }

  void onScroll() {
    // print('scroll: ${scrollController.offset}');
  }

  Widget buildBody() {
    return IndexedStack(
      index: currentTabIndex,
      children: [
        Login(),
        ForgotPassword(),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    List<IconButton> actions = [];

    if (widget.dataIconRights != null) {
      for (var i in widget.dataIconRights!) {
        actions.add(IconButton(
          onPressed: () {
            print('action');
          },
          icon: Icon(i),
        ));
      }
    }

    return MaterialApp(
      home: Scaffold(
          backgroundColor: widget.backgroundColorBody ??
              MainComponent.defaultBackgroundColorBody,
          drawer: Drawer(
            elevation: 0,
            child: Scaffold(
              appBar: AppBar(
                elevation: 0,
                leading: Container(),
              ),
              body: Container(
                color: Colors.amber,
              ),
            ),
          ),
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: currentTabIndex,
            onTap: (index) {
              setState(() {
                currentTabIndex = index;
              });
            },
            items: const [
              BottomNavigationBarItem(
                  icon: Icon(Icons.arrow_back_ios),
                  label: '111',
                  tooltip: '21312321'),
              BottomNavigationBarItem(
                icon: Icon(Icons.arrow_back_ios_new_rounded),
                label: '2222',
                tooltip: '213213',
              ),
            ],
          ),
          appBar: AppBar(
            elevation: widget.elevation,
            title: widget.title,
            centerTitle: widget.centerTitle,
            backgroundColor:
                widget.backgroundColorAppBar ?? HexColor(color_blue_1),
            // leading: Container(),
            // leading: IconButton(
            //   onPressed: () {
            //     final canPop = Navigator.of(context) // check xem có back lại màn trước đc k
            //     .canPop();
            // if (canPop) {
            //   Navigator.of(context).pop();
            // }
            //   },
            //   icon: Icon(
            //     widget.dataIconLeft,
            //     color: widget.colorIconLeft,
            //   ),
            // ),
            // leading: BackButton(
            //   color: widget.colorIconLeft,
            //   onPressed: () {
            //     final canPop = Navigator.of(context).canPop();
            //     if (canPop) {
            //       Navigator.of(context).pop();
            //     }
            //   },
            // ),
            actions: actions,
          ),
          body: GestureDetector(
            onTap: () => hideKeyboardAndUnFocus(context),
            behavior: HitTestBehavior.translucent,
            child: widget.hasSingleScrollView
                ? singleChildScrollView()
                : notSingleChildScrollView(),
          )),
    );
  }

  Widget singleChildScrollView() {
    return SingleChildScrollView(
      controller: scrollController,
      child: widget.body,
    );
  }

  Widget notSingleChildScrollView() {
    return widget.body;
  }
}
