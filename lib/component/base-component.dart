import 'package:flutter/material.dart';

abstract class BaseComponent extends StatelessWidget {
  const BaseComponent({super.key});
}

abstract class BaseComponentStateful extends StatefulWidget {
  const BaseComponentStateful({super.key});
}
