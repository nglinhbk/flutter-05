import 'package:cached_network_image/cached_network_image.dart';

import 'base-component.dart';
import 'package:flutter/material.dart';

abstract class BaseImage extends BaseComponent {
  const BaseImage({super.key});

  static const Color colorBackgroundLoading = Colors.grey;
  static const int durationAnimatedOpacity = 1;
  static const BoxFit filImage = BoxFit.none;

  frameBuilderImage({
    required BuildContext context,
    required Widget child,
    int? frame,
    required bool wasSynchronouslyLoaded,
    int? duration,
  }) {
    if (wasSynchronouslyLoaded) {
      return child;
    }
    return AnimatedOpacity(
      opacity: frame == null ? 0 : 1,
      duration: Duration(seconds: duration ?? durationAnimatedOpacity),
      child: child,
    );
  }

  loadingBuilderImage(
      {required BuildContext context,
      required Widget child,
      ImageChunkEvent? loadingProgress,
      AlignmentGeometry? alignment,
      required double? height,
      required double? width,
      Color? colorBackground}) {
    if (loadingProgress == null) {
      return child;
    }
    return Container(
      alignment: alignment ?? Alignment.center,
      color: colorBackground ?? colorBackgroundLoading,
      height: height,
      width: width,
      child: CircularProgressIndicator(
        value: loadingProgress.expectedTotalBytes != null
            ? loadingProgress.cumulativeBytesLoaded /
                loadingProgress.expectedTotalBytes!
            : null,
      ),
    );
  }
}

class ImageComponent extends BaseComponent {
  const ImageComponent({super.key, this.interactiveViewer = false});

  final bool interactiveViewer;

  @override
  Widget build(BuildContext context) {
    return interactiveViewer
        ? const ImageInteractiveViewer()
        : const ImageNetwork();
  }
}

class ImageInteractiveViewer extends BaseComponent {
  const ImageInteractiveViewer({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(child: InteractiveViewer(child: const ImageNetwork()));
  }
}

class ImageNetwork extends BaseComponent {
  const ImageNetwork({super.key, this.cache = true});
  final bool cache;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      height: 100,
      decoration: const BoxDecoration(
          image: DecorationImage(
              image: CachedNetworkImageProvider('https://picsum.photos/4992'))),
    );
    return Container(
      width: 100,
      height: 100,
      alignment: Alignment.center,
      clipBehavior: Clip.hardEdge,
      decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: AspectRatio(
        aspectRatio: 2,
        child: const ImageCache(
          url: 'https://picsum.photos/4992',
        ),
      ),
    );
    return Container(
        width: 150,
        height: 500,
        alignment: Alignment.center,
        color: Colors.black,
        child: cache
            ? const ImageCache(
                url: 'https://picsum.photos/4992',
                width: 270.0,
                height: 200.0,
              )
            : const ImageNotCache(
                url: 'https://picsum.photos/1500/1000',
                width: 200.0,
                height: 250.0,
              ));
  }
}

class ImageNotCache extends BaseImage {
  const ImageNotCache(
      {super.key,
      required this.url,
      this.width,
      this.height,
      this.colorBackgroundLoading,
      this.durationAnimatedOpacity,
      this.alignmentCircular,
      this.fit});
  final double? width;
  final double? height;
  final Color? colorBackgroundLoading;
  final int? durationAnimatedOpacity;
  final AlignmentGeometry? alignmentCircular;
  final BoxFit? fit;
  final String url;
  @override
  Widget build(BuildContext context) {
    return Image.network(
      url,
      height: height,
      width: width,
      fit: fit ?? BaseImage.filImage,
      frameBuilder: (BuildContext context, Widget child, int? frame,
          bool wasSynchronouslyLoaded) {
        return super.frameBuilderImage(
            context: context,
            child: child,
            frame: frame,
            wasSynchronouslyLoaded: wasSynchronouslyLoaded,
            duration: durationAnimatedOpacity);
      },
      loadingBuilder: (BuildContext context, Widget child,
          ImageChunkEvent? loadingProgress) {
        return super.loadingBuilderImage(
          context: context,
          child: child,
          loadingProgress: loadingProgress,
          alignment: alignmentCircular,
          height: height,
          width: width,
          colorBackground: colorBackgroundLoading,
        );
      },
    );
  }
}

class ImageCache extends BaseComponent {
  const ImageCache(
      {super.key,
      required this.url,
      this.width,
      this.height,
      this.alignmentCircular});

  final String url;
  final double? width;
  final double? height;
  final Alignment? alignmentCircular;

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: url,
      width: width,
      height: height,
      fit: BoxFit.cover,
    );
  }
}
