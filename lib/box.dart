import 'package:flutter/material.dart';

class BoxDemo extends StatelessWidget {
  const BoxDemo({super.key, this.height = 100, required this.color});

  final double height;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: height * 3,
      color: color,
    );
  }
}
