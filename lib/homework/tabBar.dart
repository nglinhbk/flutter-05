import 'package:flutter/material.dart';
import 'package:my_app_flutter/component/base-component.dart';

void main() {
  runApp(const TabBarDemo());
}

class TabBarDemo extends BaseComponentStateful {
  const TabBarDemo({super.key});

  @override
  State<StatefulWidget> createState() => _TabBarDemo();
}

class _TabBarDemo extends State<TabBarDemo>
    with SingleTickerProviderStateMixin {
  late TabController tabController;
  @override
  void initState() {
    tabController = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Tab bar demo'),
        ),
        body: Column(
          children: [
            const Text('dsad csadksajdlk'),
            TabBar(
              controller: tabController,
              labelColor: Colors.red,
              labelStyle: const TextStyle(fontSize: 20),
              unselectedLabelColor: Colors.blue,
              tabs: [
                Container(
                  color: Colors.amber,
                  child: const Text(
                    'tab 1',
                  ),
                ),
                const Text(
                  'tab 2',
                ),
              ],
              isScrollable: true, // cho scrollable
              onTap: (index) {
                print(index);
              },
            ),
            TextButton(
                onPressed: () {
                  tabController.animateTo(1); // chuyển đến tab nào
                },
                child: const Text('chuyển')),
            Expanded(
              child: TabBarView(
                controller: tabController,
                children: const [
                  TabItem(text: 'tab 1'),
                  TabItem(text: 'tab 2'),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class TabItem extends BaseComponentStateful {
  const TabItem({super.key, required this.text});

  final String text;

  @override
  State<StatefulWidget> createState() => _TabItem();
}

class _TabItem extends State<TabItem> with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Text(widget.text);
  }

  @override
  bool get wantKeepAlive => true;
}
