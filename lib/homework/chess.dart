import 'package:flutter/material.dart';

import '../component/base-component.dart';
import 'main.dart';

void main() {
  runApp(const Main(
    body: Chess(),
    title: Text('Chess'),
  ));
}

class Chess extends BaseComponentStateful {
  const Chess({super.key});

  @override
  State<StatefulWidget> createState() => _Chess();
}

class _Chess extends State<Chess> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      color: Colors.blueGrey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          chess(),
        ],
      ),
    );
  }

  Widget chess() {
    return GridView.builder(
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 8,
      ),
      itemBuilder: (context, index) {
        return itemChess(index);
      },
      itemCount: 64,
      shrinkWrap: true,
    );
  }

  Widget itemChess(int index) {
    int row = index ~/ 8;
    bool isBlack = false;
    if (row % 2 == 0 && index % 2 == 0) {
      isBlack = true;
    } else if (row % 2 != 0 && index % 2 == 1) {
      isBlack = true;
    }

    return Container(
      color: isBlack ? Colors.black : Colors.white,
    );
  }
}
