import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'main.dart';

void main() {
  runApp(const Main(body: FormLogin(), title: Text('form login demo')));
}

class FormLogin extends StatelessWidget {
  const FormLogin({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      alignment: Alignment.center,
      child: Column(
        children: [
          Container(
            margin: const EdgeInsets.symmetric(vertical: 20),
            child: const Text(
              'Đăng nhập',
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
          const TextFieldDemo(
              hintText: 'Type your phone number',
              inputType: TextInputType.phone,
              textInputAction: TextInputAction.next),
          const TextFieldDemo(
              hintText: 'Type your password',
              suffixIcon: Icon(Icons.remove_red_eye_rounded),
              typePassword: true,
              textInputAction: TextInputAction.go),
          Container(
              width: double.infinity,
              alignment: Alignment.center,
              height: 44,
              decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.circular(22),
                boxShadow: [
                  BoxShadow(
                    color: Colors.red.withOpacity(0.2),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: const Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: const Text(
                'Tiếp tục',
                style: TextStyle(color: Colors.white),
              )),
        ],
      ),
    );
  }
}

class TextFieldDemo extends StatelessWidget {
  const TextFieldDemo(
      {super.key,
      this.hintText,
      this.suffixIcon,
      this.typePassword = false,
      this.inputType = TextInputType.text,
      this.textInputAction = TextInputAction.none});

  final String? hintText;
  final Icon? suffixIcon;
  final bool typePassword;
  final TextInputType inputType;
  final TextInputAction textInputAction;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(bottom: 20),
        child: TextField(
          obscureText: typePassword,
          obscuringCharacter: '*',
          keyboardType: inputType,
          textInputAction: textInputAction,
          inputFormatters: [LengthLimitingTextInputFormatter(10)],
          decoration: InputDecoration(
              suffixIcon: suffixIcon,
              hintText: hintText,
              isCollapsed: true,
              contentPadding:
                  const EdgeInsets.symmetric(vertical: 16, horizontal: 16),
              border: const OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black54))),
        ));
  }
}
