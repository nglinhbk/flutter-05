import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../component/base-component.dart';
import 'main.dart';

void main() {
  runApp(Main(body: ListRow(), title: const Text('List row')));
}

class ListRow extends BaseComponentStateful {
  ListRow({super.key});

  final List<Map> books = [
    {
      'title': 'Vì tôi là người giúp việc! 2: bí ẩn của nhà Mitsurugi',
      'image':
          'https://books.google.com/books/publisher/content/images/frontcover/vKuUAwAAQBAJ?fife=w512-h512',
      'price': '1.99'
    },
    {
      'title': 'Vì tôi là người giúp việc! 2: bí ẩn của nhà Mitsurugi',
      'image':
          'https://books.google.com/books/publisher/content/images/frontcover/vKuUAwAAQBAJ?fife=w512-h512',
      'price': '1.99'
    },
    {
      'title': 'Vì tôi là người giúp việc! 2: bí ẩn của nhà Mitsurugi',
      'image':
          'https://books.google.com/books/publisher/content/images/frontcover/vKuUAwAAQBAJ?fife=w512-h512',
      'price': '1.99'
    },
    {
      'title': 'Vì tôi là người giúp việc! 2: bí ẩn của nhà Mitsurugi',
      'image':
          'https://books.google.com/books/publisher/content/images/frontcover/vKuUAwAAQBAJ?fife=w512-h512',
      'price': '1.99'
    },
    {
      'title': 'Vì tôi là người giúp việc! 2: bí ẩn của nhà Mitsurugi',
      'image':
          'https://books.google.com/books/publisher/content/images/frontcover/vKuUAwAAQBAJ?fife=w512-h512',
      'price': '1.99'
    },
    {
      'title': 'Vì tôi là người giúp việc! 2: bí ẩn của nhà Mitsurugi',
      'image':
          'https://books.google.com/books/publisher/content/images/frontcover/vKuUAwAAQBAJ?fife=w512-h512',
      'price': '1.99'
    },
    {
      'title': 'Vì tôi là người giúp việc! 2: bí ẩn của nhà Mitsurugi',
      'image':
          'https://books.google.com/books/publisher/content/images/frontcover/vKuUAwAAQBAJ?fife=w512-h512',
      'price': '1.99'
    },
    {
      'title': 'Vì tôi là người giúp việc! 2: bí ẩn của nhà Mitsurugi',
      'image':
          'https://books.google.com/books/publisher/content/images/frontcover/vKuUAwAAQBAJ?fife=w512-h512',
      'price': '1.99'
    },
  ];

  @override
  State<StatefulWidget> createState() => _ListRow();
}

class _ListRow extends State<ListRow> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        itemRow(),
        itemRow(),
      ],
    );
  }

  Widget itemRow() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          titleWidget('Sách bán chạy nhất'),
          listBook(),
        ],
      ),
    );
  }

  Widget titleWidget(String text) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(text, style: const TextStyle(fontSize: 20)),
          const Icon(Icons.arrow_forward),
        ],
      ),
    );
  }

  Widget listBook() {
    return SizedBox(
      height: 250,
      child: ListView.builder(
        itemBuilder: (context, index) {
          final book = widget.books[index];
          return itemBook(book);
        },
        itemCount: widget.books.length,
        scrollDirection: Axis.horizontal, // nếu là ngang thì phải có chiều cao
      ),
    );
  }

  Widget itemBook(Map book) {
    return Container(
      width: 130,
      padding: const EdgeInsets.only(right: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CachedNetworkImage(
            imageUrl: book['image']!,
          ),
          Text(
            book['title'],
            style: const TextStyle(fontSize: 16),
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          ),
          Text(
            book['price'],
            style: const TextStyle(fontSize: 16),
          ),
        ],
      ),
    );
  }
}
