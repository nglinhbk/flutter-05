import 'package:flutter/material.dart';

import 'main.dart';

void main() {
  runApp(const Main(body: InputDemo(), title: Text('input demo')));
}

// class InputDemo extends StatefulWidget {
//   const InputDemo({super.key});

//   @override
//   State<StatefulWidget> createState() => _InputDemoState();
// }

// class _InputDemoState extends State<InputDemo> {
//   final controller = TextEditingController();
//   final focus = FocusNode();

//   @override
//   void initState() {
//     super.initState();
//     // focus.requestFocus();
//     // controller.text = 'test';
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       margin: const EdgeInsets.only(top: 100),
//       child: Column(
//         children: [
//           TextField(
//             controller: controller,
//             focusNode: focus,
//             autofocus: true,
//             minLines: 1,
//             // maxLines: 5,
//             keyboardType: TextInputType.text,
//             textCapitalization: TextCapitalization.none,
//             textAlign: TextAlign.end,
//             readOnly: false,
//             obscureText: false, // password
//             obscuringCharacter: '*', // obscureText = true thì hoạt động
//             autocorrect: false, // k cho tự động chỉnh sửa
//             enableSuggestions: false, // k hiển thị sợ ý
//             enableInteractiveSelection: true, // cho phép select or pate
//             onChanged: (v) {
//               // khi người dùng thay đổi text thì hàm này sẽ chạy
//               // khi code thay đổi value thì k chạy vào hàm này
//             },
//             onSubmitted: (v) {
//               print('submit');
//             },
//             textInputAction:
//                 TextInputAction.send, // hành động của nút submit trên bàn phím
//             decoration: const InputDecoration(
//                 labelText: 'Name',
//                 hintText: 'Enter your name',
//                 labelStyle: TextStyle(color: Colors.amber),
//                 contentPadding: EdgeInsets.all(10),
//                 border: OutlineInputBorder(
//                     borderSide: BorderSide(color: Colors.red))),
//           ),
//           IconButton(
//             icon: const Icon(Icons.add),
//             onPressed: () {},
//           )
//         ],
//       ),
//     );
//   }
// }
class InputDemo extends StatelessWidget {
  const InputDemo({super.key, required this.controller});

  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 100),
      color: Colors.amber,
      height: 100,
      child: const TextField(
        decoration: InputDecoration(
            border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.black))),
      ),
    );
  }
}
