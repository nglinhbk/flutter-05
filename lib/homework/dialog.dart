import 'package:flutter/material.dart';
import 'package:my_app_flutter/component/base-component.dart';

void main() {
  runApp(const DialogDemo());
}

class DialogDemo extends BaseComponentStateful {
  const DialogDemo({super.key});

  @override
  State<StatefulWidget> createState() => _DialogDemo();
}

class _DialogDemo extends State<DialogDemo> {
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: DialogCp(),
    );
  }
}

class DialogCp extends BaseComponentStateful {
  const DialogCp({super.key});

  @override
  State<StatefulWidget> createState() => _DialogCp();
}

class _DialogCp extends State<DialogCp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('dialog'),
      ),
      body: GestureDetector(
        onTap: () {
          showDialog(
            // routeSettings: RouteSettings(),
            useSafeArea: false,
            barrierColor: Colors.black54, // background color
            barrierDismissible:
                true, // cho phép click ra ngoài là tắt dialog hay không
            context: context,
            builder: (context) {
              return Dialog(
                child: Container(
                  height: 200,
                  width: MediaQuery.sizeOf(context).width - 64,
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(16)),
                  ),
                ),
              );
            },
          );
        },
        child: const Text('click open dialog'),
      ),
    );
  }
}
