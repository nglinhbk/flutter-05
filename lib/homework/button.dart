import 'package:flutter/material.dart';

import 'main.dart';

void main() {
  runApp(const Main(body: ButtonDemo(), title: Text('button demo')));
}

class ButtonDemo extends StatelessWidget {
  const ButtonDemo({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        IconButton(
            onPressed: () {
              print('1111');
            },
            icon: const Icon(Icons.add)),
        TextButton(
          onPressed: () {
            print('2222');
          },
          child: const Text('button text'),
        ),
        ElevatedButton(
            onPressed: () {
              print('3333');
            },
            child: const Text('ElevatedButton')),
        OutlinedButton(
            // style: ButtonStyle(padding: EdgeInsets.all(10)),
            onPressed: () {
              print('4444');
            },
            child: const Text('OutlinedButton')),
        InkWell(
          onTap: () {
            print('5555');
          },
          child: Container(
            width: 100,
            height: 100,
            color: Colors.amber,
          ),
        ),
        GestureDetector(
          onTap: () {
            print('6666');
          },
          behavior: HitTestBehavior.translucent,
          child: Container(
            child: Text('GestureDetector'),
          ),
        )
      ],
    );
  }
}
