import 'package:flutter/material.dart';

import '../component/button.dart';
import '../component/input.dart';
import '../component/main.dart';
import 'lesson9.dart';
// import 'main.dart';

void main() {
  runApp(const MainComponent(
      dataIconRights: [Icons.logout],
      centerTitle: false,
      body: Lesson10(),
      title: Text('Thông tin cá nhân')));
}

class Lesson10 extends StatelessWidget {
  const Lesson10({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      color: const Color.fromARGB(255, 230, 230, 230),
      child: Column(
        children: [
          const Lesson9(
            name: 'Linh Nguyễn',
          ),
          const InputComponent(
            label: 'Họ & tên',
            hintText: 'Nhập tên của bạn',
            autocorrect: false,
            textCapitalization: TextCapitalization.words,
            textInputAction: TextInputAction.next,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Expanded(
                child: InputComponent(
                  label: 'Ngày sinh',
                  hintText: 'Nhập ngày sinh của bạn',
                  suffixInput: Icon(Icons.date_range_outlined),
                  inputType: TextInputType.datetime,
                  textInputAction: TextInputAction.next,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 10),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: const EdgeInsets.only(bottom: 7),
                      child: const Text(
                        'Giới tính',
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.w500),
                      ),
                    ),
                    SizedBox(
                      width: 150,
                      height: 50,
                      child: Row(
                        children: [
                          Expanded(
                              child: Container(
                                  decoration: BoxDecoration(
                                      color: Colors.black,
                                      border: Border.all(
                                          color: const Color.fromARGB(
                                              134, 139, 138, 138)),
                                      borderRadius: const BorderRadius.only(
                                          topLeft: Radius.circular(8),
                                          bottomLeft: Radius.circular(8))),
                                  alignment: Alignment.center,
                                  child: const Text(
                                    'Nam',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.white),
                                  ))),
                          Expanded(
                            child: Container(
                                decoration: BoxDecoration(
                                    color: Colors.transparent,
                                    border: Border.all(
                                        color: const Color.fromARGB(
                                            134, 139, 138, 138)),
                                    borderRadius: const BorderRadius.only(
                                        topRight: Radius.circular(8),
                                        bottomRight: Radius.circular(8))),
                                alignment: Alignment.center,
                                height: double.infinity,
                                child: const Text('Nữ',
                                    textAlign: TextAlign.center)),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
          const InputComponent(
            label: 'Email (không bắt buộc)',
            hintText: 'Nhập email của bạn',
            textInputAction: TextInputAction.next,
            inputType: TextInputType.emailAddress,
          ),
          InputComponent(
            label: 'Địa chỉ',
            hintText: 'Nhập địa chỉ của bạn',
            textInputAction: TextInputAction.send,
            onSubmitted: (_) => save(),
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: const ButtonComponent(
              label: 'Lưu thay đổi',
              fontWeightLabel: FontWeight.bold,
              onPressed: save,
            ),
          ),
        ],
      ),
    );
  }
}

void save() {
  print('click button');
}
