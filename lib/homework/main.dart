import 'package:flutter/material.dart';

import '../helper/keyboard.dart';

class Main extends StatelessWidget {
  const Main(
      {super.key,
      required this.body,
      required this.title,
      this.dataIconLeft = Icons.arrow_back,
      this.dataIconRights,
      this.backgroundColorAppBar,
      this.centerTitle = true});

  final Widget body;
  final Text title;
  final IconData dataIconLeft;
  final List<IconData>? dataIconRights;
  final Color? backgroundColorAppBar;
  final bool centerTitle;

  @override
  Widget build(BuildContext context) {
    List<IconButton> actions = [];

    if (dataIconRights != null) {
      for (var i in dataIconRights!) {
        actions.add(IconButton(
          onPressed: () {
            print('action');
          },
          icon: Icon(i),
        ));
      }
    }

    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: title,
            centerTitle: centerTitle,
            backgroundColor: backgroundColorAppBar ?? Colors.red.shade700,
            leading: IconButton(
              onPressed: () {
                print('back');
              },
              icon: Icon(dataIconLeft),
            ),
            actions: actions,
          ),
          body: GestureDetector(
            onTap: () => hideKeyboardAndUnFocus(context),
            behavior: HitTestBehavior.translucent,
            child: body,
          )),
    );
  }
}
