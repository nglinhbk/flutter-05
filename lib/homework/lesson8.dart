import 'package:flutter/material.dart';

import 'main.dart';

void main() {
  runApp(const Main(body: Lesson8(), title: Text('Lesson 8')));
}

class Lesson8 extends StatelessWidget {
  const Lesson8({super.key});

  static const List data = [
    {'title': 'Chắn', 'count': 5},
    {'title': 'Lẻ', 'count': 2},
    {'title': 'Hòa CL', 'count': 3},
  ];

  @override
  Widget build(BuildContext context) {
    int max = 0;
    List<Widget> rows = [];
    for (var i in data) {
      if (i['count'] > max) max = i['count'];
    }
    for (var i in data) {
      rows.add(ItemList(
          text: i['title'], count: i['count'], percent: i['count'] / max));
    }
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: rows,
      ),
    );
  }
}

class ItemList extends StatelessWidget {
  const ItemList(
      {super.key,
      required this.text,
      required this.count,
      required this.percent});
  static const double height = 40;
  final String text;
  final int count;
  final double percent;

  @override
  Widget build(BuildContext context) {
    double heightLine = height / 5;

    if (height <= 5) heightLine = 5;

    int percentContainer = percent >= 1 ? 100 : (percent * 100).toInt();

    return Container(
      decoration: const BoxDecoration(
        border: Border(bottom: BorderSide(color: Colors.grey, width: 0.5)),
      ),
      child: Row(
        children: [
          SizedBox(
            width: 70,
            // padding: const EdgeInsets.only(right: 10.0),
            height: height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  text,
                  // textAlign: TextAlign.left,
                  style: const TextStyle(fontSize: 15),
                ),
              ],
            ),
          ),
          Expanded(
            child: SizedBox(
              height: height,
              child: Row(
                children: [
                  Expanded(
                      flex: percentContainer,
                      child: Container(
                        height: heightLine,
                        decoration: const BoxDecoration(
                            color: Colors.deepOrange,
                            borderRadius:
                                BorderRadius.all(Radius.circular(50))),
                      )),
                  Expanded(flex: 100 - percentContainer, child: Container()),
                ],
              ),
            ),
          ),
          SizedBox(
            width: 70,
            // padding: const EdgeInsets.only(left: 10.0),
            height: height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  '$count lần',
                  // textAlign: TextAlign.right,
                  style: const TextStyle(fontSize: 15),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
