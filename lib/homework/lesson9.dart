import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'main.dart';

void main() {
  runApp(const Main(
      body: Lesson9(
          name: 'Linh Nguyễn',
          avatarUrl:
              'https://scontent.fhan5-11.fna.fbcdn.net/v/t39.30808-6/350487495_207933268744648_8811140322303266311_n.jpg?stp=dst-jpg_s720x720&_nc_cat=1&ccb=1-7&_nc_sid=8bfeb9&_nc_ohc=WyiPyRwhIBQAX_m4Ajw&_nc_ht=scontent.fhan5-11.fna&oh=00_AfD2dWKCWCIhONYfHOtAeTBK5Z7TEfPP_UTSb3VlQkmsTA&oe=647E393D'),
      title: Text('Lesson 9')));
}

class AvatarByName extends StatelessWidget {
  const AvatarByName({super.key, required this.name});

  final String name;

  @override
  Widget build(BuildContext context) {
    String firstCharacter =
        name.isNotEmpty ? name[0].toUpperCase() : 'Not avatar';
    return Container(
      alignment: Alignment.center,
      decoration: const BoxDecoration(
        color: Colors.red,
      ),
      child: Text(
        firstCharacter,
        style:
            TextStyle(color: Colors.white, fontSize: name.isNotEmpty ? 35 : 20),
      ),
    );
  }
}

class AvatarByImage extends StatelessWidget {
  const AvatarByImage({super.key, required this.name, required this.url});

  final String name;
  final String url;

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: url,
      placeholder: (context, url) => const CircularProgressIndicator(),
      errorWidget: (context, url, error) => AvatarByName(
        name: name,
      ),
    );
  }
}

class Lesson9 extends StatelessWidget {
  const Lesson9(
      {super.key, required this.name, this.avatarUrl, this.isUpload = true});

  final String name;
  final String? avatarUrl;
  final bool isUpload;
  final double width = 100;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      // padding: const EdgeInsets.all(16),
      margin: const EdgeInsets.all(16),
      decoration: const BoxDecoration(
          // color: Colors.black12,
          borderRadius: BorderRadius.all(Radius.circular(8))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            width: width,
            margin: const EdgeInsets.only(bottom: 15),
            child: Stack(children: [
              AspectRatio(
                aspectRatio: 1,
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(width / 2),
                      border: Border.all(color: Colors.white)),
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(width / 2),
                      child: avatarUrl != null
                          ? AvatarByImage(name: name, url: avatarUrl!)
                          : AvatarByName(name: name)),
                ),
              ),
              Positioned(
                bottom: 5,
                right: 0,
                child: Visibility(
                  visible: isUpload,
                  child: Container(
                    width: 30,
                    height: 30,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: Colors.white12),
                        borderRadius:
                            const BorderRadius.all(Radius.circular(15))),
                    child: const Icon(
                      Icons.upload,
                      color: Colors.red,
                    ),
                  ),
                ),
              ),
            ]),
          ),
          const Text(
            '0987654321',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 24),
          )
        ],
      ),
    );
  }
}
