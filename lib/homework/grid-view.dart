import 'package:flutter/material.dart';

import '../component/base-component.dart';
import 'main.dart';

void main() {
  runApp(const Main(body: GridViewDemo(), title: Text('GridView Demo')));
}

class GridViewDemo extends BaseComponentStateful {
  const GridViewDemo({super.key});

  @override
  State<StatefulWidget> createState() => _GridViewDemo();
}

class _GridViewDemo extends State<GridViewDemo> {
  @override
  Widget build(BuildContext context) {
    return buildWidget();
  }

  Widget buildWidget() {
    final width = MediaQuery.of(context).size.width;
    print(width);

    var crossAxisCount = 3;
    if (width > 500) crossAxisCount = 5;

    const itemWidth = 64;

    crossAxisCount = width ~/ itemWidth;

    // return GridView(
    //   gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
    //     crossAxisCount: crossAxisCount, // có bao nhiêu cột
    //     mainAxisSpacing: 16, // khoảng cách ngang
    //     crossAxisSpacing: 8, // khoảng cách dọc
    //     childAspectRatio: 2, // tỷ lý width/height
    //   ),
    //   children: [
    //     blueBox(),
    //     blueBox(),
    //     blueBox(),
    //     blueBox(),
    //     blueBox(),
    //     blueBox(),
    //     blueBox(),
    //     blueBox(),
    //     blueBox(),
    //   ],
    // );

    return GridView(
      gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 50,
        mainAxisSpacing: 16, // khoảng cách ngang
        crossAxisSpacing: 8, // khoảng cách dọc
        childAspectRatio: 1, // tỷ lý width/height
      ),
      children: [
        blueBox(),
        blueBox(),
        blueBox(),
        blueBox(),
        blueBox(),
      ],
    );
  }

  Widget blueBox() {
    return Container(
      // width: 64,
      // height: 64, // k có tác dụng width height khi dùng GridView
      decoration: const BoxDecoration(
          color: Colors.blue,
          borderRadius: BorderRadius.all(Radius.circular(50))),
    );
  }
}
