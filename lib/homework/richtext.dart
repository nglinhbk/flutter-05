import 'package:flutter/material.dart';

import 'main.dart';

void main() {
  runApp(const Main(body: RichtextDemo(), title: Text('richtext demo')));
}

class RichtextDemo extends StatelessWidget {
  const RichtextDemo({super.key});

  @override
  Widget build(BuildContext context) {
    return RichText(
        text: const TextSpan(style: TextStyle(color: Colors.amber), children: [
      TextSpan(text: 'test 1111', style: TextStyle(color: Colors.black)),
      TextSpan(text: 'test 222', style: TextStyle(fontSize: 30)),
    ]));
  }
}
