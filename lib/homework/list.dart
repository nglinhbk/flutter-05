import 'package:flutter/material.dart';

import '../component/base-component.dart';
import 'main.dart';

void main() {
  runApp(const Main(
    body: ListDemo(),
    title: Text('List Demo'),
  ));
}

class ListDemo extends BaseComponentStateful {
  const ListDemo({super.key});

  @override
  State<StatefulWidget> createState() => _ListDemo();
}

class _ListDemo extends State<ListDemo> {
  final users = <String>[
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
    'g',
    'h',
    'i',
    'j',
    'k',
    'l',
    'm',
  ];

  // final

  @override
  Widget build(BuildContext context) {
    // return ListView.builder(
    //   itemBuilder: (context, index) {
    //     final user = users[index];

    //     return buildItem(index, user);
    //   },
    //   itemCount: users.length,
    // );

    return ListView.separated(
      separatorBuilder: (_, __) => Divider(),
      itemBuilder: (context, index) {
        final user = users[index];

        return buildItem(index, user);
      },
      itemCount: users.length,
      // physics: const NeverScrollableScrollPhysics(),
    );
  }

  Widget buildItem(int index, String user) {
    final odd = index % 2 != 0;
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 40),
      // color: odd ? Colors.yellow : Colors.white,
      child: Row(
        children: [
          SizedBox(
            width: 32,
            child: Text('$index'),
          ),
          Expanded(
            child: Text(user),
          ),
        ],
      ),
    );
  }
}
