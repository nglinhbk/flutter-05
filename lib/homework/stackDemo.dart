import 'package:flutter/material.dart';

import 'main.dart';

void main() {
  runApp(const Main(body: StackDemo(), title: Text('stack demo')));
}

class StackDemo extends StatelessWidget {
  const StackDemo({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          color: Colors.blue,
          width: double.infinity,
          height: 200,
          margin: const EdgeInsets.only(bottom: 10),
          child: Stack(
            alignment: AlignmentDirectional.center,
            // textDirection: TextDirection.ltr,
            children: [
              Positioned(
                left: 100,
                top: 0,
                bottom: 0,
                child: Container(
                  width: 50,
                  color: Colors.yellow,
                ),
              ),
              Positioned(
                left: 0,
                right: 0,
                child: Container(
                  height: 50,
                  color: Colors.yellow,
                ),
              )
            ],
          ),
        ),
        SizedBox(
          height: 200,
          width: double.infinity,
          child: Stack(alignment: AlignmentDirectional.center, children: [
            Positioned.fill(
              // Positioned.fill là đưa các khối con to bằng khối cha
              child: Column(children: [
                Expanded(
                  child: Container(
                    color: Colors.red,
                  ),
                ),
                Expanded(
                  child: Container(
                    color: Colors.blue,
                  ),
                )
              ]),
            ),
            const Positioned(
                child: Icon(
              Icons.star_rate,
              color: Colors.yellow,
              size: 150,
            ))
          ]),
        ),
      ],
    );
  }
}
