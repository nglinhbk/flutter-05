import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:my_app_flutter/component/base-component.dart';

void main() {
  runApp(const LoadingDemo());
}

class LoadingDemo extends BaseComponentStateful {
  const LoadingDemo({super.key});

  @override
  State<StatefulWidget> createState() => _LoadingDemo();
}

class _LoadingDemo extends State<LoadingDemo> {
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: LoadingCp(),
    );
  }
}

class LoadingCp extends BaseComponentStateful {
  const LoadingCp({super.key});

  @override
  State<StatefulWidget> createState() => _LoadingCp();
}

class _LoadingCp extends State<LoadingCp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('loading'),
      ),
      body: GestureDetector(
        onTap: () {
          showLoading(context);
        },
        child: const Text('click open loading'),
      ),
    );
  }

  void showLoading(context) {
    showDialog(
      // routeSettings: RouteSettings(),
      useSafeArea: false,
      barrierColor: Colors.black54, // background color
      barrierDismissible:
          true, // cho phép click ra ngoài là tắt dialog hay không
      context: context,
      builder: (context) {
        return Dialog(
          backgroundColor: Colors.transparent,
          elevation: 0,
          child: Container(
            width: 100,
            height: 100,
            alignment: Alignment.center,
            child: Lottie.asset('assets/json/7731-water-loading.json'),
          ),
        );
      },
    );
  }
}
