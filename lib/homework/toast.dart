import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:lottie/lottie.dart';
import 'package:my_app_flutter/component/base-component.dart';
import 'package:http/http.dart' as http;
import 'package:http/retry.dart';
import 'package:my_app_flutter/helper/client.dart';

void main() {
  runApp(const ToastDemo());
}

class ToastDemo extends BaseComponentStateful {
  const ToastDemo({super.key});

  @override
  State<StatefulWidget> createState() => _ToastDemo();
}

class _ToastDemo extends State<ToastDemo> {
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: ToastCp(),
    );
  }
}

class ToastCp extends BaseComponentStateful {
  const ToastCp({super.key});

  @override
  State<StatefulWidget> createState() => _ToastCp();
}

class _ToastCp extends State<ToastCp> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    call();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('loading'),
      ),
      body: GestureDetector(
        onTap: showToast,
        child: const Text('show'),
      ),
    );
  }

  Future<void> call() async {
    try {
      Response data = await MyClient()
          .get(Uri.parse('https://api.quynhtao.com/api/userssss'));
      // print(data.statusCode);
    } finally {}
  }

  OverlayEntry? overlayEntry;
  void showToast() {
    overlayEntry = OverlayEntry(
      builder: (context) {
        return Positioned(
          top: 64,
          left: 32,
          child: GestureDetector(
            onTap: () {
              remove();
            },
            child: Container(
              width: 200,
              height: 200,
              color: Colors.amber,
            ),
          ),
        );
      },
    );

    final OverlayState overlayState = Overlay.of(context);

    overlayState.insert(overlayEntry!);
  }

  void remove() {
    overlayEntry?.remove();
  }
}
