import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'main.dart';
import '../component/image.dart';

void main() {
  runApp(const Main(body: ImageDemo(), title: Text('image demo')));
}

class ImageDemo extends StatelessWidget {
  const ImageDemo({super.key});

  @override
  Widget build(BuildContext context) {
    return const ImageComponent();
    // return Image.network(
    //   "https://scontent.fhan3-3.fna.fbcdn.net/v/t39.30808-6/350502395_1338771646854859_4153910935361545013_n.jpg?stp=dst-jpg_p180x540&_nc_cat=101&ccb=1-7&_nc_sid=730e14&_nc_ohc=66RPULtoVsQAX8242j4&_nc_ht=scontent.fhan3-3.fna&oh=00_AfDjnCvb4J_MJCec9qh3TunlnQCtXS6liW5r5el4RDaGJw&oe=647E1617",
    //   loadingBuilder: (_, __, ___) {
    //     return __;
    //   },
    // );
    // return SizedBox(
    //   height: 100,
    //   width: 100,
    //   child: Image.asset(
    //     'assets/images/image.png',
    //     // width: double.infinity,
    //     // height: 300,
    //     // fit: BoxFit.scaleDown,
    //   ),
    // );
    return InteractiveViewer(
      child: CachedNetworkImage(
        imageUrl:
            "https://scontent.fhan3-3.fna.fbcdn.net/v/t39.30808-6/350502395_1338771646854859_4153910935361545013_n.jpg?stp=dst-jpg_p180x540&_nc_cat=101&ccb=1-7&_nc_sid=730e14&_nc_ohc=66RPULtoVsQAX8242j4&_nc_ht=scontent.fhan3-3.fna&oh=00_AfDjnCvb4J_MJCec9qh3TunlnQCtXS6liW5r5el4RDaGJw&oe=647E1617",
        placeholder: (context, url) {
          return const CircularProgressIndicator();
        },
      ),
    );
  }
}
