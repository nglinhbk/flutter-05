import 'package:flutter/material.dart';

import '../component/base-component.dart';
import '../component/button.dart';
import '../component/main.dart';

void main() {
  runApp(const MainComponent(
    body: Cards(),
    title: Text('Cards'),
  ));
}

class Cards extends BaseComponentStateful {
  const Cards({super.key});

  @override
  State<StatefulWidget> createState() => _Cards();
}

class _Cards extends State<Cards> {
  final List<Map> data = [
    {
      "key": 1,
      "showImage": false,
      "image": "assets/images/cards/cat1.jpeg",
    },
    {
      "key": 1,
      "showImage": false,
      "image": "assets/images/cards/cat2.jpeg",
    },
    {
      "key": 2,
      "showImage": false,
      "image": "assets/images/cards/chicken1.jpeg",
    },
    {
      "key": 2,
      "showImage": false,
      "image": "assets/images/cards/chicken2.jpeg",
    },
    {
      "key": 3,
      "showImage": false,
      "image": "assets/images/cards/dog1.jpeg",
    },
    {
      "key": 3,
      "showImage": false,
      "image": "assets/images/cards/dog2.jpeg",
    },
    {
      "key": 4,
      "showImage": false,
      "image": "assets/images/cards/pic.png",
    },
    {
      "key": 4,
      "showImage": false,
      "image": "assets/images/cards/pic3.jpg",
    },
    {
      "key": 4,
      "showImage": false,
      "image": "assets/images/cards/pic3.jpg",
    },
  ];

  bool notActionClick = false;

  int? valueCheck;
  List<int> listClick = [];

  @override
  void initState() {
    super.initState();
    data.shuffle();
  }

  void handleClick(Map card, int index) async {
    if (valueCheck == null) {
      valueCheck = card['key'];
      listClick = [index];
    } else {
      if (valueCheck != card['key']) {
        // chọn sai

        notActionClick = true;
        setState(() {});
        await Future.delayed(const Duration(seconds: 1));

        for (var k in listClick) {
          data[k]['showImage'] = false;
        }
        data[index]['showImage'] = false;
        valueCheck = null;
        listClick = [];

        notActionClick = false;
        setState(() {});
      } else {
        listClick.add(index);

        int checkNumber = 0;
        for (Map element in data) {
          if (element['key'] == card['key']) {
            checkNumber++;
          }
        }

        if (checkNumber == listClick.length) {
          valueCheck = null;
          listClick = [];
        }
      }
    }
  }

  void refresh() {
    if (notActionClick) return;
    notActionClick = false;
    listClick = [];
    valueCheck = null;

    for (var element in data) {
      element['showImage'] = false;
    }

    data.shuffle();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      // color: Colors.amberAccent,
      child: Column(
        children: [
          GridView.builder(
            physics: const NeverScrollableScrollPhysics(),
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3,
              mainAxisSpacing: 10,
              crossAxisSpacing: 10,
            ),
            itemBuilder: (context, index) {
              final card = data[index];
              return itemCard(card, index, notActionClick);
            },
            itemCount: data.length,
            shrinkWrap: true,
          ),
          Container(
            padding: const EdgeInsets.only(bottom: 16),
            child: ButtonComponent(
              label: 'Chơi lại',
              onPressed: refresh,
            ),
          ),
        ],
      ),
    );
  }

  Widget itemCard(Map card, int index, bool notActionClick) {
    return ItemCard(
      card: card,
      notActionClick: notActionClick,
      onClick: () => handleClick(card, index),
    );
  }
}

class ItemCard extends BaseComponentStateful {
  const ItemCard({
    super.key,
    required this.card,
    required this.onClick,
    required this.notActionClick,
  });

  final Map card;
  final VoidCallback onClick;
  final bool notActionClick;

  @override
  State<StatefulWidget> createState() => _ItemCard();
}

class _ItemCard extends State<ItemCard> {
  @override
  Widget build(BuildContext context) {
    bool showImage = widget.card["showImage"]!;
    String urlImage = widget.card["image"];
    return GestureDetector(
      onTap: clickTab,
      child: Container(
        decoration: BoxDecoration(
          color: !showImage ? Colors.blue : null,
          borderRadius: const BorderRadius.all(Radius.circular(30)),
          border: showImage ? Border.all(color: Colors.black45) : null,
        ),
        child: Visibility(
          visible: showImage,
          child: ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(30)),
            child: Image.asset(
              urlImage,
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }

  void clickTab() {
    if (widget.card["showImage"] || widget.notActionClick) return;

    widget.card["showImage"] = true;
    setState(() {});
    widget.onClick();
  }
}
