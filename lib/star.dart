import 'package:flutter/material.dart';

class StarDemo extends StatelessWidget {
  const StarDemo({super.key, this.star = 0});

  final int star;

  @override
  Widget build(BuildContext context) {
    List<Icon> icons = [];
    for (int i = 1; i <= 5; i++) {
      icons.add(Icon(
        Icons.star,
        color: (i <= star ? Colors.yellow.shade900 : Colors.grey),
        size: 50.0,
      ));
    }

    return SizedBox(
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: icons,
      ),
    );
  }
}
