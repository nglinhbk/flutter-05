import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:http/retry.dart';

class MyClient extends http.BaseClient {
  MyClient();
  Map<String, String>? _defaultHeaders;
  final http.Client _httpClient = RetryClient(
    http.Client(),
    retries: 2,
    whenError: (p0, p1) {
      print(p0);
      print(p1);
      return true;
    },
    when: (response) async {
      if (response.statusCode == 404) {
        print('Unauthorized');
        await Future.delayed(const Duration(seconds: 3));
      }
      return true;
    },
  );
  @override
  Future<http.StreamedResponse> send(http.BaseRequest request) {
    return _httpClient.send(request);
  }

  @override
  Future<Response> get(url, {Map<String, String>? headers}) {
    return _httpClient.get(url, headers: _mergedHeaders(headers));
  }

  Map<String, String> _mergedHeaders(Map<String, String>? headers) =>
      {...?_defaultHeaders, ...?headers};
}
