import 'package:flutter/material.dart';

void push(BuildContext context, Widget page) {
  Navigator.of(context).push(
    MaterialPageRoute(builder: (ctx) => page),
  );
}
