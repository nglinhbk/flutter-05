import 'package:flutter/material.dart';
import 'box.dart';

class ColumeDemo extends StatelessWidget {
  const ColumeDemo({super.key});

  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    List<Color> colors = [
      Colors.white,
      Colors.red,
      Colors.green,
    ];
    // ignore: unused_local_variable
    List<BoxDemo> children = [];

    // ignore: unused_local_variable
    for (var color in colors) {
      children.add(BoxDemo(color: color));
    }

    return SizedBox(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            decoration: BoxDecoration(border: Border.all(color: Colors.black)),
            child: Column(
              children: children,
            ),
          ),
        ],
      ),
    );
  }
}
