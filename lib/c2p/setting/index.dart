import 'package:flutter/material.dart';
import 'package:my_app_flutter/component/base-component.dart';
import 'package:my_app_flutter/component/button.dart';

import '../../component/main.dart';
import '../library/index.dart';
import '../login.dart';
import 'infoUser.dart';
import 'listSetting.dart';

void main() {
  runApp(const MainComponent(
    title: Text("Settings"),
    body: Setting(),
  ));
}

class Setting extends BaseComponentStateful {
  const Setting({super.key});

  @override
  State<StatefulWidget> createState() => _Setting();
}

class _Setting extends State<Setting> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: content());
  }

  Widget content() {
    return Container(
      padding: const EdgeInsets.all(16),
      width: double.infinity,
      child: Column(
        children: [
          const Info(),
          const ListSetting(),
          test(),
        ],
      ),
    );
  }

  Widget test() {
    return Column(
      children: [
        ButtonComponent(
          label: 'library',
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(builder: (ctx) => const Library()),
            );
          },
        ),
        ButtonComponent(
          label: 'logout',
          onPressed: () {
            Navigator.of(context).popUntil((route) => route.isFirst);
          },
        ),
        ButtonComponent(
          label: 'màn thứ 2',
          onPressed: () {
            Navigator.of(context)
                .popUntil((route) => route.settings.name == 'ForgotPassword');
          },
        ),
        ButtonComponent(
          label: 'Back',
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }
}
