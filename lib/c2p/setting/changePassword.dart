import 'package:flutter/material.dart';
import 'package:my_app_flutter/component/base-component.dart';

import '../../component/main.dart';

void main() {
  runApp(const MainComponent(
    title: Text("Change password"),
    body: ChangePassword(),
  ));
}

class ChangePassword extends BaseComponentStateful {
  const ChangePassword({super.key});

  @override
  State<StatefulWidget> createState() => _ChangePassword();
}

class _ChangePassword extends State<ChangePassword> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
