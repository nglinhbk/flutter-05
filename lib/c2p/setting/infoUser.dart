import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:my_app_flutter/common/color.dart';

import '../../component/base-component.dart';
import '../components/avatar.dart';

class Info extends BaseComponentStateful {
  const Info({super.key});

  @override
  State<StatefulWidget> createState() => _Info();
}

class _Info extends State<Info> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(14),
      decoration: BoxDecoration(
        color: HexColor(color_white_100),
        borderRadius: const BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          BoxShadow(
            color: HexColor(color_black).withOpacity(0.2),
            spreadRadius: 4,
            blurRadius: 8,
            offset: const Offset(0, 3), // changes position of shadow
          )
        ],
      ),
      child: Column(
        children: [
          avatarAndName(),
          info(),
        ],
      ),
    );
  }

  Widget avatarAndName() {
    return Container(
      padding: const EdgeInsets.only(bottom: 16),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(color: HexColor(color_grey_text)),
        ),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 16),
            child: const Avatar(
              width: 87,
              upload: true,
            ),
          ),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  margin: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    'Brewen Le Hai',
                    style: TextStyle(
                      color: HexColor(color_black),
                      fontWeight: FontWeight.w600,
                      fontSize: 22,
                    ),
                  ),
                ),
                RichText(
                    text: TextSpan(
                        text: 'Role: ',
                        style: TextStyle(
                          color: HexColor(color_black_60),
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                        ),
                        children: [
                      TextSpan(
                        text: 'Cold Chain Manager',
                        style: TextStyle(
                            color: HexColor(color_black_60),
                            fontSize: 16,
                            fontWeight: FontWeight.w500),
                      )
                    ]))
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget info() {
    List<Widget> info = [
      itemInfo('Date of birth', '11 Jan 1995'),
      itemInfo('Gende', 'Male'),
      itemInfo('Workplace', '143 Doc Ngu, Ha Noi'),
    ];
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 16),
      child: Column(children: info),
    );
  }

  Widget itemInfo(String label, String value) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 3),
      child: Row(
        children: [
          Text(
            '$label:',
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w700,
                color: HexColor(color_black_60)),
          ),
          const Padding(padding: EdgeInsets.only(left: 5)),
          Expanded(
            child: Text(
              value,
              style: TextStyle(
                color: HexColor(color_black_60),
                fontSize: 16,
                fontWeight: FontWeight.w500,
              ),
            ),
          )
        ],
      ),
    );
  }
}
