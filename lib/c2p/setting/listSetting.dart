import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:my_app_flutter/common/color.dart';
import 'package:my_app_flutter/component/base-component.dart';

import '../../common/icon.dart';
import '../components/switchComponent.dart';

class ListSetting extends BaseComponentStateful {
  const ListSetting({super.key});

  @override
  State<StatefulWidget> createState() => _ListSetting();
}

class _ListSetting extends State<ListSetting> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 16),
      child: Column(
        children: [
          itemSetting(
            label: 'Role',
            select: true,
            valueSelect: 'Coachee',
            urlIcon: icon_role,
          ),
          itemSetting(
            label: 'Notification',
            showSwich: true,
            valueSwich: true,
            urlIcon: icon_notidication,
          ),
          itemSetting(
            label: 'Language',
            select: true,
            valueSelect: 'English',
            urlIcon: icon_language,
          ),
          itemSetting(
            label: 'Change password',
            urlIcon: icon_change_password,
          ),
          itemSetting(
            label: 'Edit profile',
            urlIcon: icon_edit_profile,
          ),
          itemSetting(
            label: 'Block',
            urlIcon: icon_block,
          ),
          itemSetting(
            label: 'Log out',
            urlIcon: icon_logout,
            showRigth: false,
          ),
        ],
      ),
    );
  }

  Widget itemSetting({
    required String label,
    required String urlIcon,
    bool select = false,
    String? valueSelect,
    bool showRigth = true,
    bool showSwich = false,
    bool valueSwich = true,
  }) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: showSwich ? 6 : 16),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Image.asset(
            urlIcon,
            width: 17,
          ),
          Expanded(
            child: Container(
              padding: const EdgeInsets.only(left: 14),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    label,
                    style: TextStyle(
                      color: HexColor(color_black_60),
                      fontSize: 17,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  Visibility(
                    visible: showRigth,
                    child: Row(
                      children: [
                        Visibility(
                          visible: select,
                          child: Container(
                            margin: const EdgeInsets.only(right: 15),
                            child: Text(
                              valueSelect ?? '',
                              style: TextStyle(
                                color: HexColor(color_blue_1),
                                fontSize: 15,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ),
                        Visibility(
                          visible: !showSwich,
                          child: Icon(
                            Icons.arrow_forward_ios_rounded,
                            size: 17,
                            color: HexColor(color_grey_text),
                          ),
                        ),
                        Visibility(
                          visible: showSwich,
                          child: SwichComponent(
                              value: valueSwich,
                              onChanged: (value) {
                                print(value);
                              }),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
