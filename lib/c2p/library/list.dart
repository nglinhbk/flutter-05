import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:my_app_flutter/common/color.dart';
import 'package:my_app_flutter/component/base-component.dart';

import '../../component/main.dart';
import '../components/filterDocument.dart';
import '../components/itemDocument.dart';

void main() {
  runApp(const MainComponent(
    hasSingleScrollView: false,
    title: Text('E-JobAids'),
    body: ListLibrary(),
    dataIconRights: [Icons.search, Icons.notifications, Icons.message],
  ));
}

class ListLibrary extends BaseComponentStateful {
  const ListLibrary({super.key});

  @override
  State<StatefulWidget> createState() => _ListLibrary();
}

class _ListLibrary extends State<ListLibrary> {
  final int _count = 20;

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: refresh,
      child: Container(padding: const EdgeInsets.all(16), child: list()),
    );
  }

  Widget title() {
    return Text(
      'Drink milk tea in the hot summer and go travel in Bali this year.',
      style: TextStyle(
        color: HexColor(color_blue_1),
        fontSize: 18,
        fontWeight: FontWeight.w600,
      ),
    );
  }

  Widget list() {
    return ListView.builder(
      itemBuilder: (context, index) {
        if (index == 0) {
          return Column(
            children: [
              title(),
              const FilterDocument(),
            ],
          );
        }
        return Container(
          margin: const EdgeInsets.only(bottom: 30),
          child: const ItemDocument(),
        );
      },
      itemCount: _count + 1,
      physics: const ScrollPhysics(),
    );
  }

  Future<void> refresh() async {
    print('refresh');
  }
}
