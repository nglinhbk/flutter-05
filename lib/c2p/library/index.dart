import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:my_app_flutter/common/color.dart';
import 'package:my_app_flutter/component/base-component.dart';

import '../../component/main.dart';

void main() {
  runApp(const MainComponent(
    title: Text('Library'),
    body: Library(),
    dataIconRights: [Icons.search, Icons.notifications, Icons.message],
  ));
}

class Library extends BaseComponentStateful {
  const Library({super.key});

  @override
  State<StatefulWidget> createState() => _Library();
}

class _Library extends State<Library> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            item(
              title: 'E-JobAids',
              colorTitle: HexColor(color_white_100),
              colorBackground: HexColor(color_blue_1),
              description:
                  'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
            ),
            item(
              title: 'Documents resources',
              colorTitle: HexColor(color_black_90),
              colorBackground: HexColor(color_blue_2),
              description:
                  'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
            ),
          ],
        ),
      ),
    );
  }

  Widget item({
    required String title,
    required Color colorTitle,
    required Color colorBackground,
    required String description,
  }) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 28),
      margin: const EdgeInsets.symmetric(vertical: 18),
      width: double.infinity,
      decoration: BoxDecoration(
        border: Border.all(color: HexColor(color_black_60)),
        borderRadius: const BorderRadius.all(Radius.circular(12)),
        color: colorBackground,
      ),
      child: Column(
        children: [
          Stack(
            children: [
              Container(
                width: double.infinity,
                margin: const EdgeInsets.only(bottom: 20),
                child: Text(
                  title,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: colorTitle,
                    fontSize: 22,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              Positioned(
                right: 0,
                child: Icon(
                  Icons.arrow_forward,
                  size: 28,
                  color: colorTitle,
                ),
              )
            ],
          ),
          Text(
            description,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: colorTitle,
              fontWeight: FontWeight.w400,
              fontSize: 13,
            ),
          )
        ],
      ),
    );
  }
}
