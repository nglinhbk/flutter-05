import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:my_app_flutter/c2p/components/itemCategory.dart';
import 'package:my_app_flutter/common/color.dart';
import 'package:my_app_flutter/component/base-component.dart';

import '../../component/loading-dot.dart';
import '../../component/main.dart';

void main() {
  runApp(const MainComponent(
    hasSingleScrollView: false,
    title: Text('E-JobAids'),
    body: ListCategory(),
    dataIconRights: [Icons.notifications, Icons.message],
  ));
}

class ListCategory extends BaseComponentStateful {
  const ListCategory({super.key});

  @override
  State<StatefulWidget> createState() => _ListCategory();
}

class _ListCategory extends State<ListCategory> {
  final int _count = 15;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      child: RefreshIndicator(
        onRefresh: refresh,
        child: ListView.builder(
          itemBuilder: (context, index) {
            if (index < _count - 1) {
              return ItemCategory();
            } else {
              return LoadingDot(
                color: HexColor(color_blue_1),
              );
            }
          },
          itemCount: _count,
        ),
      ),
    );
  }

  Future<void> refresh() async {
    print('refresh');
  }
}
