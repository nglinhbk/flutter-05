import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:my_app_flutter/common/color.dart';
import 'package:my_app_flutter/component/base-component.dart';

class RadioComponent extends BaseComponentStateful {
  const RadioComponent({
    super.key,
    required this.options,
    this.value,
    this.onChanged,
  });

  final List<Map<String, dynamic>> options;
  final String? value;
  final Function(String)? onChanged;

  @override
  State<StatefulWidget> createState() => _RadioComponent();
}

class _RadioComponent extends State<RadioComponent> {
  String? _value;

  @override
  void initState() {
    super.initState();
    _value = widget.value;
  }

  @override
  void didUpdateWidget(covariant RadioComponent oldWidget) {
    super.didUpdateWidget(oldWidget);
    _value = widget.value;
  }

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemBuilder: (context, index) {
        final option = widget.options[index];
        String value = option['value'].toString();
        Widget? label = option['label'];

        return itemRadio(value: value, label: label);
      },
      separatorBuilder: (_, __) =>
          const Padding(padding: EdgeInsets.only(bottom: 5)),
      itemCount: widget.options.length,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
    );
  }

  Widget itemRadio({required var value, Widget? label}) {
    return Row(
      children: [
        Radio(
          activeColor: HexColor(color_blue_1),
          visualDensity: const VisualDensity(
              horizontal: VisualDensity.minimumDensity,
              vertical: VisualDensity.minimumDensity),
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          value: value,
          groupValue: _value,
          onChanged: (value) => change(value),
        ),
        Expanded(
          child: GestureDetector(
            onTap: () => change(value),
            child: Container(
              margin: const EdgeInsets.only(left: 6),
              child: label,
            ),
          ),
        )
      ],
    );
  }

  void change(value) {
    setState(() {
      _value = value.toString();
    });
    widget.onChanged?.call(value);
  }
}
