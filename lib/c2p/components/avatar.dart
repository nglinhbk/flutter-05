import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:my_app_flutter/component/base-component.dart';

import '../../common/color.dart';
import '../../common/icon.dart';
import '../../common/image.dart';
import '../../helper/image.dart';

class Avatar extends BaseComponentStateful {
  const Avatar({
    super.key,
    this.width = 100,
    this.url,
    this.upload = false,
  });
  final double width;
  final String? url;
  final bool upload;

  @override
  State<StatefulWidget> createState() => _Avatar();
}

class _Avatar extends State<Avatar> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: widget.width,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(widget.width / 2)),
              border: Border.all(color: HexColor(color_black_40))),
          child: AspectRatio(
            aspectRatio: 1,
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(widget.width / 2)),
              child: widget.url != null ? avatar() : notAvatar(),
            ),
          ),
        ),
        Positioned(
          bottom: 0,
          right: 0,
          child: Visibility(
            visible: widget.upload,
            child: Container(
              width: 26,
              decoration: BoxDecoration(
                  color: HexColor(color_white_100),
                  borderRadius: const BorderRadius.all(Radius.circular(13)),
                  border: Border.all(color: HexColor(color_grey_text))),
              child: AspectRatio(
                aspectRatio: 1,
                child: ClipRRect(child: Image.asset(icon_camera)),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget notAvatar() {
    return Image.asset(
      image_not_avatar,
      fit: BoxFit.cover,
    );
  }

  Widget avatar() {
    return CachedNetworkImage(
      imageUrl: widget.url!,
      fit: BoxFit.cover,
      placeholder: (context, url) => loadingImage(),
      errorWidget: (context, url, error) => notAvatar(),
    );
  }
}
