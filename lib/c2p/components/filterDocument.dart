import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:my_app_flutter/component/base-component.dart';
import 'package:my_app_flutter/component/input.dart';

import '../../common/color.dart';
import '../../common/icon.dart';

class FilterDocument extends BaseComponentStateful {
  const FilterDocument({super.key});

  @override
  State<StatefulWidget> createState() => _FilterDocument();
}

class _FilterDocument extends State<FilterDocument> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 16),
      child: Column(
        children: [
          inputSearch(),
          tabSeach(),
        ],
      ),
    );
  }

  Widget inputSearch() {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: HexColor(color_grey_text)),
        borderRadius: const BorderRadius.all(Radius.circular(8)),
      ),
      width: double.infinity,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 35,
            width: 36,
            alignment: Alignment.centerRight,
            child: Image.asset(
              icon_search,
            ),
          ),
          Expanded(
            child: InputComponent(
              onlyTextField: true,
              hintText: 'Keyword...',
              heightInput: 34,
              fillColorInput: HexColor(color_white_100),
              focusedBorder: InputBorder.none,
            ),
          ),
          iconFilter(),
        ],
      ),
    );
  }

  Widget iconFilter() {
    return Container(
      width: 43,
      height: 37,
      decoration: BoxDecoration(
        color: HexColor(color_grey_box),
        borderRadius: const BorderRadius.only(
          topRight: Radius.circular(8),
          bottomRight: Radius.circular(8),
        ),
      ),
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(color: HexColor(color_grey_text)),
          ),
        ),
        child: Icon(
          Icons.filter_alt_outlined,
          color: HexColor(color_black_60),
        ),
      ),
    );
  }

  Widget tabSeach() {
    List<Widget> tabSearchs = List.filled(5, itemTabSeach());
    return Container(
      margin: const EdgeInsets.only(top: 16),
      width: double.infinity,
      child: Wrap(
        spacing: 16,
        runSpacing: 16,
        children: tabSearchs,
      ),
    );
  }

  Widget itemTabSeach() {
    return Container(
      height: 36,
      padding: const EdgeInsets.symmetric(horizontal: 12),
      decoration: BoxDecoration(
          color: HexColor(color_bg_tab),
          borderRadius: const BorderRadius.all(Radius.circular(18))),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                'Tiếng anh',
                style: TextStyle(
                  color: HexColor(color_black_60),
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                ),
              ),
              IconButton(
                padding: EdgeInsets.zero,
                constraints: const BoxConstraints(),
                onPressed: () {},
                icon: Icon(
                  Icons.close,
                  size: 16,
                  color: HexColor(color_black_60),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
