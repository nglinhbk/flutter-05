import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:my_app_flutter/common/color.dart';
import 'package:my_app_flutter/component/base-component.dart';

class CheckBoxComponent extends BaseComponentStateful {
  const CheckBoxComponent({
    super.key,
    this.values,
    this.onChanged,
    this.width = 18,
    this.disable = false,
    required this.options,
  });

  final List<Map> options;
  final List<dynamic>? values;
  final Function(List<dynamic>)? onChanged;
  final double width;
  final bool disable;

  @override
  State<StatefulWidget> createState() => _CheckBoxComponent();
}

class _CheckBoxComponent extends State<CheckBoxComponent> {
  List<dynamic> _listChecked = [];
  @override
  void initState() {
    super.initState();
    _listChecked = widget.values ?? [];
  }

  @override
  void didUpdateWidget(covariant CheckBoxComponent oldWidget) {
    super.didUpdateWidget(oldWidget);
    _listChecked = widget.values ?? [];
  }

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemBuilder: (context, index) {
        final Map option = widget.options[index];
        var value = option['value'];
        Widget? label = option['label'];
        return itemCheckbox(value: value, label: label);
      },
      separatorBuilder: (_, __) =>
          const Padding(padding: EdgeInsets.only(bottom: 10)),
      itemCount: widget.options.length,
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
    );
  }

  Widget itemCheckbox({required var value, Widget? label}) {
    bool isChecked = _listChecked.contains(value);
    return Row(
      children: [
        SizedBox(
          width: widget.width,
          child: AspectRatio(
            aspectRatio: 1,
            child: Checkbox(
              activeColor: HexColor(color_blue_1),
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(3)),
              ),
              side: MaterialStateBorderSide.resolveWith(
                (states) => isChecked
                    ? BorderSide.none
                    : BorderSide(
                        width: 1,
                        color: HexColor(color_black_60),
                      ),
              ),
              value: isChecked,
              onChanged: (bool? _) => changeValue(value, _!),
            ),
          ),
        ),
        Expanded(
          child: GestureDetector(
            onTap: () => changeValue(value, !isChecked),
            child: Container(
              margin: const EdgeInsets.only(left: 6),
              child: label,
            ),
          ),
        )
      ],
    );
  }

  void changeValue(var valueAdd, bool add) {
    if (widget.disable) return;

    if (add) {
      _listChecked.add(valueAdd);
    } else {
      _listChecked.removeWhere((item) => item == valueAdd);
    }
    setState(() {});
    widget.onChanged?.call(_listChecked);
  }
}
