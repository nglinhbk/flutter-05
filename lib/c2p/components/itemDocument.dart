import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:my_app_flutter/common/color.dart';
import 'package:my_app_flutter/component/base-component.dart';

import '../../common/icon.dart';

class ItemDocument extends BaseComponentStateful {
  const ItemDocument({
    super.key,
    this.showNewVersion = true,
    this.showLike = true,
    this.showBadge = true,
    this.showDownload = true,
    this.showShare = true,
  });

  final bool showNewVersion;
  final bool showLike;
  final bool showBadge;
  final bool showDownload;
  final bool showShare;

  @override
  State<StatefulWidget> createState() => _ItemDocument();
}

class _ItemDocument extends State<ItemDocument> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        image(),
        info(),
      ],
    );
  }

  Widget image() {
    double height = 165;
    return Container(
      height: height,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(12)),
        image: DecorationImage(
            fit: BoxFit.cover,
            image: CachedNetworkImageProvider(
                'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/640px-Image_created_with_a_mobile_phone.png')),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            padding: const EdgeInsets.only(top: 4, left: 16, right: 3),
            width: double.infinity,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Visibility(
                  visible: widget.showNewVersion,
                  child: Container(
                    height: 25,
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: HexColor(color_bg_new_version),
                      border: Border.all(color: HexColor(color_blue_1)),
                      borderRadius: const BorderRadius.all(Radius.circular(5)),
                    ),
                    child: Text(
                      'New version',
                      style: TextStyle(
                        color: HexColor(color_blue_1),
                        fontSize: 13,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
                Visibility(
                  visible: widget.showLike,
                  child: IconButton(
                    onPressed: () {},
                    icon: Image.asset(
                      icon_like,
                      width: 20,
                    ),
                  ),
                )
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.only(bottom: 4, left: 16, right: 3),
            width: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Visibility(
                  visible: widget.showBadge,
                  child: Image.asset(icon_badge),
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Visibility(
                      visible: widget.showDownload,
                      child: IconButton(
                        onPressed: () {},
                        icon: Icon(
                          Icons.download,
                          color: HexColor(color_black_60),
                          size: 30,
                        ),
                      ),
                    ),
                    Visibility(
                      visible: widget.showShare,
                      child: IconButton(
                        padding: const EdgeInsets.only(bottom: 5),
                        onPressed: () {},
                        icon: Image.asset(icon_share),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget info() {
    List<Widget> tabs = List.filled(5, tab('Tiếng Anh'));
    return Container(
      margin: const EdgeInsets.only(top: 10),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'GaneshAID và những người đồng nghiệp',
            style: TextStyle(
              color: HexColor(color_black_60),
              fontSize: 16,
              fontWeight: FontWeight.w600,
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 16),
            child: Wrap(
              spacing: 16,
              runSpacing: 16,
              children: tabs,
            ),
          ),
        ],
      ),
    );
  }

  Widget tab(String tab) {
    return Container(
      height: 36,
      padding: const EdgeInsets.symmetric(horizontal: 12),
      decoration: BoxDecoration(
          color: HexColor(color_bg_tab),
          borderRadius: const BorderRadius.all(Radius.circular(18))),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            tab,
            style: TextStyle(
              color: HexColor(color_black_60),
              fontSize: 12,
              fontWeight: FontWeight.w400,
            ),
          ),
        ],
      ),
    );
  }
}
