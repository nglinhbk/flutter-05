import 'package:flutter/material.dart';
import 'package:my_app_flutter/component/base-component.dart';

class SwichComponent extends BaseComponentStateful {
  const SwichComponent({
    super.key,
    required this.value,
    this.onChanged,
  });

  final bool value;
  final Function(bool)? onChanged;

  @override
  State<StatefulWidget> createState() => _SwichComponent();
}

class _SwichComponent extends State<SwichComponent> {
  bool valueSwich = true;
  @override
  void initState() {
    super.initState();
    valueSwich = widget.value;
  }

  @override
  Widget build(BuildContext context) {
    return Switch(
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      value: valueSwich,
      onChanged: (bool value) {
        setState(() {
          valueSwich = value;
        });
        widget.onChanged?.call(value);
      },
    );
  }
}
