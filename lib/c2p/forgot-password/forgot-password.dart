import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import '../../component/button.dart';
import '../../component/input.dart';

import '../../common/color.dart';
import '../../component/base-component.dart';
import '../../component/main.dart';
import '../setting/index.dart';

void main() {
  runApp(MainComponent(
    backgroundColorAppBar: Colors.white,
    // elevation: 0,
    colorIconLeft: HexColor(color_blue_1),
    body: const ForgotPassword(),
  ));
}

class ForgotPassword extends BaseComponentStateful {
  const ForgotPassword({
    super.key,
    this.email,
    this.onBack,
  });
  final String? email;
  final Function(String)? onBack;

  @override
  State<StatefulWidget> createState() => _ForgotPassword();
}

class _ForgotPassword extends State<ForgotPassword> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: content());
  }

  Widget content() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(widget.email ?? ''),
          text(),
          FormSendEmail(onBack: widget.onBack),
        ],
      ),
    );
  }

  Widget text() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(bottom: 24),
          child: Text(
            'Reset your password',
            style: TextStyle(
              fontSize: 26,
              fontWeight: FontWeight.w500,
              color: HexColor(color_blue_1),
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(bottom: 14),
          child: Text(
            'Forgot your password?',
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 18,
              color: HexColor(color_black_60),
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(bottom: 20),
          child: Text(
            'Please enter the email address associated with your email. We will email you a link to reset your password.',
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w500,
              color: HexColor(color_black_60),
            ),
          ),
        ),
      ],
    );
  }
}

class FormSendEmail extends BaseComponentStateful {
  const FormSendEmail({super.key, this.onBack});
  final Function(String)? onBack;

  @override
  State<StatefulWidget> createState() => _FormSendEmail();
}

class _FormSendEmail extends State<FormSendEmail> {
  @override
  Widget build(BuildContext context) {
    String email = '';
    bool loading = false;
    return Column(
      children: [
        InputComponent(
          value: email,
          hintText: 'Nhập email',
          inputType: TextInputType.emailAddress,
          textInputAction: TextInputAction.send,
        ),
        ButtonComponent(
          label: 'Send',
          loading: loading,
          onPressed: send,
        ),
      ],
    );
  }

  void send() async {
    print('xử lý');
    // Navigator.of(context).push(
    // // push này k xóa khỏi stack
    //   MaterialPageRoute(builder: (ctx) => const Setting()),
    // );
    // Navigator.of(context).pushReplacement(
    //   // xóa màn này khỏi stack
    //   MaterialPageRoute(builder: (ctx) => const Setting()),
    // );

    // back lại và truyền data
    widget.onBack?.call('11111');
  }
}
