import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:my_app_flutter/component/base-component.dart';
import 'package:my_app_flutter/component/button.dart';
import 'package:my_app_flutter/component/input.dart';

import '../../common/color.dart';
import '../../component/main.dart';

void main() {
  runApp(MainComponent(
    backgroundColorAppBar: Colors.white,
    // elevation: 0,
    colorIconLeft: HexColor(color_blue_1),
    body: const UpdatePassword(),
  ));
}

class UpdatePassword extends BaseComponentStateful {
  const UpdatePassword({super.key});

  @override
  State<StatefulWidget> createState() => _UpdatePassword();
}

class _UpdatePassword extends State<UpdatePassword> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Update password',
            style: TextStyle(
                color: HexColor(color_blue_1),
                fontSize: 26,
                fontWeight: FontWeight.w500),
          ),
          const FormUpdatePassword(),
        ],
      ),
    );
  }
}

class FormUpdatePassword extends BaseComponentStateful {
  const FormUpdatePassword({super.key});

  @override
  State<StatefulWidget> createState() => _FormUpdatePassword();
}

class _FormUpdatePassword extends State<FormUpdatePassword> {
  String password = '';
  String passwordConfirm = '';
  bool loading = false;

  Map<String, String> errors = {};

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 28),
      child: Column(
        children: [
          InputComponent(
            value: password,
            onChanged: (value) => password = value,
            typePassword: true,
            hintText: 'New password',
            errorMessage: errors['password'],
          ),
          InputComponent(
            value: passwordConfirm,
            onChanged: (value) => passwordConfirm = value,
            typePassword: true,
            hintText: 'Confirm password',
            errorMessage: errors['passwordConfirm'],
          ),
          ButtonComponent(
            label: 'Update',
            loading: loading,
            onPressed: updatePassword,
          )
        ],
      ),
    );
  }

  void updatePassword() async {
    loading = true;
    setState(() {});

    await Future.delayed(const Duration(seconds: 2));

    loading = false;
    setState(() {});
  }
}
