import 'dart:async';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:my_app_flutter/component/button.dart';
import 'package:my_app_flutter/component/input.dart';

import '../../common/color.dart';
import '../../component/base-component.dart';
import '../../component/main.dart';
import '../../helper/keyboard.dart';

// link figma https://www.figma.com/file/EYChhwxWCKJ8GE1SX95SV2/Make-C2P-great-again!?type=design&node-id=74-1047&t=okyYUhVVxwZnHlgn-4
void main() {
  runApp(MainComponent(
    backgroundColorAppBar: Colors.white,
    // elevation: 0,
    colorIconLeft: HexColor(color_blue_1),
    body: const VerifyCode(),
  ));
}

class VerifyCode extends BaseComponentStateful {
  const VerifyCode({super.key});

  @override
  State<StatefulWidget> createState() => _VerifyCode();
}

class _VerifyCode extends State<StatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          text(),
          const FormCode(),
        ],
      ),
    );
  }

  Widget text() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(bottom: 24),
          child: Text(
            'Verify your mobile',
            style: TextStyle(
              fontSize: 26,
              fontWeight: FontWeight.w500,
              color: HexColor(color_blue_1),
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(bottom: 14),
          child: Text(
            'Enter OTP code',
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 18,
              color: HexColor(color_black_60),
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(bottom: 20),
          child: Text(
            'A verification code has been sent to +84 888 8888, enter it below',
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w500,
              color: HexColor(color_black_60),
            ),
          ),
        ),
      ],
    );
  }
}

class FormCode extends BaseComponentStateful {
  const FormCode({super.key});

  @override
  State<StatefulWidget> createState() => _FormCode();
}

class _FormCode extends State<FormCode> {
  int lengthCode = 4;
  double crossAxisSpacing = 16;
  bool loading = false;
  bool sentCode = false;
  int secondResend = 10;
  Timer? timer;

  List code = [];

  @override
  void initState() {
    super.initState();
    for (int i = 0; i < lengthCode; i++) {
      if (!code.asMap().containsKey(i)) {
        code.add('');
      }
    }
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: lengthCode,
            crossAxisSpacing: crossAxisSpacing,
            childAspectRatio: 1,
          ),
          itemBuilder: (context, index) {
            return itemInputCode(index);
          },
          itemCount: code.length,
          shrinkWrap: true,
        ),
        Container(
          margin: const EdgeInsets.only(bottom: 23),
          child: ButtonComponent(
            label: sentCode ? 'Verify code' : 'Send',
            onPressed: sendCode,
            loading: loading,
          ),
        ),
        resendCode(),
      ],
    );
  }

  Widget itemInputCode(int index) {
    String value = code[index].toString();
    return InputComponent(
      value: value,
      onChanged: (value) => changeValue(index, value),
      textAlign: TextAlign.center,
      inputType: TextInputType.number,
      enableInteractiveSelection: false,
      onlyTextField: true,
      autocorrect: false,
    );
  }

  Widget resendCode() {
    TextSpan resendCode = TextSpan(
        text: 'Resend code',
        style: TextStyle(
            color: HexColor(color_blue_1),
            fontSize: 16,
            fontWeight: FontWeight.w600),
        recognizer: TapGestureRecognizer()..onTap = fnResendCode);

    TextSpan awaitResendCode = TextSpan(
      text: 'Resend code(${secondResend}s)',
      style: TextStyle(
          color: HexColor(color_black_60),
          fontSize: 16,
          fontWeight: FontWeight.w600),
    );

    TextSpan textResend =
        sentCode && secondResend == 0 ? resendCode : awaitResendCode;

    return Visibility(
      visible: sentCode,
      child: RichText(
        text: TextSpan(
          text: 'Didn’t receive SMS?',
          style: TextStyle(
            color: HexColor(color_black_60),
            fontSize: 16,
            fontWeight: FontWeight.w400,
          ),
          children: [
            const WidgetSpan(
              child: Padding(
                padding: EdgeInsets.only(left: 5),
              ),
            ),
            textResend
          ],
        ),
      ),
    );
  }

  void changeValue(int index, String value) {
    value = value.trim();
    if (value == '') return;
    code[index] = value.characters.last;
    if (index < lengthCode - 1) {
      FocusScope.of(context).nextFocus();
    } else {
      hideKeyboardAndUnFocus(context);
    }
    setState(() {});
  }

  void startTimer() {
    timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      setState(() {
        if (secondResend > 0) {
          secondResend--;
        } else {
          timer.cancel();
        }
      });
    });
  }

  void sendCode() async {
    loading = true;
    setState(() {});

    await Future.delayed(const Duration(seconds: 2));
    loading = false;
    sentCode = true;
    setState(() {});
    startTimer();
  }

  void fnResendCode() async {
    loading = true;
    sentCode = false;
    setState(() {});

    await Future.delayed(const Duration(seconds: 2));
    loading = false;
    secondResend = 10;
    sentCode = true;
    setState(() {});
    startTimer();
  }
}
