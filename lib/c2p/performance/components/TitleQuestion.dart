import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:my_app_flutter/component/base-component.dart';

import '../../../common/color.dart';

class TitleQuestion extends BaseComponent {
  const TitleQuestion({
    super.key,
    required this.title,
    this.required = false,
  });

  final String title;
  final bool required;

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        children: [
          TextSpan(
            text: '*',
            style: TextStyle(
              color: HexColor(color_red_icon),
              fontSize: 16,
              fontWeight: FontWeight.w600,
            ),
          ),
          TextSpan(
              text: title,
              style: TextStyle(
                  color: HexColor(color_black_80),
                  fontSize: 16,
                  fontWeight: FontWeight.w600))
        ],
      ),
    );
  }
}
