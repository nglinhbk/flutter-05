import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:my_app_flutter/component/base-component.dart';

import '../../../common/color.dart';
import 'question/questionCheckbox.dart';
import 'question/questionRadio.dart';

class ItemQuestion extends BaseComponentStateful {
  const ItemQuestion({
    super.key,
    required this.type,
  });

  final String type;

  @override
  State<StatefulWidget> createState() => _ItemQuestion();
}

class _ItemQuestion extends State<ItemQuestion> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(12),
      decoration: BoxDecoration(
        color: HexColor(color_white_100),
        borderRadius: const BorderRadius.all(
          Radius.circular(12),
        ),
        boxShadow: [
          BoxShadow(
            color: HexColor(color_black).withOpacity(0.2),
            spreadRadius: 2,
            blurRadius: 4,
            offset: const Offset(0, 3), // changes position of shadow
          )
        ],
      ),
      child: question(),
    );
  }

  Widget question() {
    switch (widget.type) {
      case 'checkbox':
        return QuestionCheckbox();
      case 'radio':
        return QuestionRadio();
      default:
    }
    return Container();
  }
}
