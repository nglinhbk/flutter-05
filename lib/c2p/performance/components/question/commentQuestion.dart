import 'package:flutter/material.dart';
import 'package:my_app_flutter/common/color.dart';

import '../../../../common/icon.dart';
import '../../../../component/base-component.dart';

class CommentQuestion extends BaseComponentStateful {
  const CommentQuestion({super.key});

  @override
  State<StatefulWidget> createState() => _CommentQuestion();
}

class _CommentQuestion extends State<CommentQuestion> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SizedBox(
          width: 26,
          child: Image.asset(
            icon_message,
          ),
        ),
        Positioned(
          top: 0,
          right: 0,
          child: Image.asset(
            icon_dot_blue1,
            width: 8,
          ),
        )
      ],
    );
  }
}
