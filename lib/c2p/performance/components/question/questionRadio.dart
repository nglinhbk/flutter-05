import 'package:flutter/widgets.dart';
import 'package:my_app_flutter/component/base-component.dart';

import '../../../components/radioComponent.dart';
import '../titleQuestion.dart';
import 'attachmentQuestion.dart';
import 'textAnswer.dart';

class QuestionRadio extends BaseComponentStateful {
  const QuestionRadio({super.key});

  @override
  State<StatefulWidget> createState() => _QuestionRadio();
}

class _QuestionRadio extends State<QuestionRadio> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const TitleQuestion(title: 'Quels est de couverture vaccinale ?'),
          answer(),
          const AttachmentQuestion(),
        ],
      ),
    );
  }

  Widget answer() {
    List<Map<String, dynamic>> options = [
      {
        'value': 1,
        'label': const TextAnswer(
            answer: 'Lorem Ipsum is simply dummy text printing ')
      },
      {
        'value': 2,
        'label': const TextAnswer(
            answer: 'Lorem Ipsum is simply dummy text printing ')
      },
    ];
    return Container(
      margin: const EdgeInsets.only(top: 5),
      child: Column(
        children: [
          RadioComponent(
            options: options,
            onChanged: (value) {
              print(value);
            },
          ),
        ],
      ),
    );
  }
}
