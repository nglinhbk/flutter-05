import 'package:flutter/material.dart';
import 'package:my_app_flutter/common/color.dart';

import '../../../../common/icon.dart';
import '../../../../component/base-component.dart';

class FileQuestion extends BaseComponentStateful {
  const FileQuestion({super.key});

  @override
  State<StatefulWidget> createState() => _FileQuestion();
}

class _FileQuestion extends State<FileQuestion> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SizedBox(
          width: 26,
          child: Image.asset(
            icon_image,
          ),
        ),
        Positioned(
          top: 0,
          right: 0,
          child: Image.asset(
            icon_dot_blue1,
            width: 8,
          ),
        )
      ],
    );
  }
}
