import 'package:flutter/material.dart';
import 'package:my_app_flutter/component/base-component.dart';

import '../../../../common/icon.dart';
import 'commentQuestion.dart';
import 'fileQuestion.dart';

class AttachmentQuestion extends BaseComponentStateful {
  const AttachmentQuestion({super.key});

  @override
  State<StatefulWidget> createState() => _AttachmentQuestion();
}

class _AttachmentQuestion extends State<AttachmentQuestion> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Row(
            children: [
              CommentQuestion(),
              Padding(padding: EdgeInsets.only(left: 5)),
              FileQuestion(),
            ],
          ),
          GestureDetector(
            child: Container(
              width: 19,
              child: Image.asset(icon_question_mark),
            ),
          ),
        ],
      ),
    );
  }
}
