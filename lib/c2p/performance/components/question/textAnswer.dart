import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:my_app_flutter/common/color.dart';
import 'package:my_app_flutter/component/base-component.dart';

class TextAnswer extends BaseComponent {
  const TextAnswer({
    super.key,
    required this.answer,
  });

  final String answer;

  @override
  Widget build(BuildContext context) {
    return Text(
      answer,
      style: TextStyle(
        color: HexColor(color_black_60),
        fontSize: 12,
        fontWeight: FontWeight.w500,
      ),
    );
  }
}
