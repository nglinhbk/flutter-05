import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:my_app_flutter/common/color.dart';
import 'package:my_app_flutter/component/base-component.dart';

import '../../component/main.dart';
import 'components/itemquestion.dart';

void main() {
  runApp(const MainComponent(
    hasSingleScrollView: false,
    title: Text('Performance'),
    body: Performance(),
  ));
}

class Performance extends BaseComponentStateful {
  const Performance({super.key});

  @override
  State<StatefulWidget> createState() => _Performance();
}

class _Performance extends State<Performance> {
  int _count = 3;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 16),
      child: ListView.separated(
        itemBuilder: (context, index) {
          return itemPage();
        },
        separatorBuilder: (_, __) => Container(
          height: 18,
          width: double.infinity,
        ),
        itemCount: _count,
      ),
    );
  }
}

Widget itemPage() {
  return Container(
    margin: const EdgeInsets.only(bottom: 24),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          margin: const EdgeInsets.only(bottom: 16),
          child: Text(
            'Gestion des vaccins et consommables',
            style: TextStyle(
              color: HexColor(color_blue_1),
              fontSize: 19,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: answers(),
        ),
      ],
    ),
  );
}

Widget answers() {
  return ListView.separated(
    itemBuilder: (context, index) {
      switch (index) {
        case 0:
          return const ItemQuestion(
            type: 'checkbox',
          );
        case 1:
          return const ItemQuestion(
            type: 'radio',
          );
        default:
      }
      return Container();
    },
    separatorBuilder: (_, __) => Container(
      width: double.infinity,
      height: 16,
    ),
    itemCount: 2,
    shrinkWrap: true,
    physics: const NeverScrollableScrollPhysics(),
  );
}
