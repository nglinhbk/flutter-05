import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:my_app_flutter/c2p/setting/index.dart';
import '../component/button.dart';
import '../component/input.dart';
import '../common/color.dart';
import '../common/image.dart';
import '../component/base-component.dart';
import '../component/main.dart';
import 'forgot-password/forgot-password.dart';

// link figma https://www.figma.com/file/EYChhwxWCKJ8GE1SX95SV2/Make-C2P-great-again!?type=design&node-id=60-183&t=u2m517BwN7EGK2Tq-4
void main() {
  runApp(const Login());
}

class Login extends BaseComponentStateful {
  const Login({super.key});

  @override
  State<StatefulWidget> createState() => _Login();
}

class _Login extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return MainComponent(
      // backgroundColorAppBar: Colors.white,
      elevation: 0,
      body: content(),
    );
  }

  Widget content() {
    return Column(
      children: [
        logo(),
        const FormLogin(),
        signUp(),
      ],
    );
  }

  Widget logo() {
    final widthScreen = MediaQuery.of(context).size.width;
    return SizedBox(
      width: double.infinity,
      child: SizedBox(
        width: widthScreen / 2,
        child: Image.asset(image_logo),
      ),
    );
  }

  Widget signUp() {
    return RichText(
        text: TextSpan(
            text: "Don't have an account?",
            style: TextStyle(
              color: HexColor(color_black),
              fontSize: 16,
            ),
            children: [
          const WidgetSpan(
            child: Padding(
              padding: EdgeInsets.only(left: 5),
            ),
          ),
          TextSpan(
            text: "Sign up",
            style: TextStyle(
                color: HexColor(color_blue_1),
                fontSize: 16,
                fontWeight: FontWeight.w600),
          )
        ]));
  }
}

// ignore: must_be_immutable
class FormLogin extends BaseComponentStateful {
  const FormLogin({super.key});

  @override
  State<StatefulWidget> createState() => _FormLogin();
}

class _FormLogin extends State<FormLogin> {
  String email = '';
  String password = '';
  bool loading = false;

  Map<String, String> errors = {};

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.only(bottom: 27),
            child: Text(
              'Login',
              style: TextStyle(
                color: HexColor(color_blue_1),
                fontWeight: FontWeight.w500,
                fontSize: 26,
              ),
            ),
          ),
          InputComponent(
            value: email,
            onChanged: (value) => email = value,
            errorMessage: errors['email'],
            hintText: 'Email',
            inputType: TextInputType.emailAddress,
            textInputAction: TextInputAction.next,
          ),
          InputComponent(
            value: password,
            onChanged: (value) => password = value,
            onSubmitted: (_) => submit.call(),
            errorMessage: errors['password'],
            hintText: 'Password',
            typePassword: true,
            textInputAction: TextInputAction.send,
          ),
          Container(
            alignment: Alignment.topRight,
            margin: const EdgeInsets.only(bottom: 20),
            child: TextButton(
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (ctx) => ForgotPassword(
                      email: '11111',
                      onBack: (data) {
                        print(data);
                      },
                    ),
                    settings: const RouteSettings(name: 'ForgotPassword'),
                  ),
                );
              },
              child: Text(
                'Forgot your password?',
                style: TextStyle(
                  color: HexColor(color_black_60),
                  fontWeight: FontWeight.w300,
                  fontSize: 12,
                ),
              ),
            ),
          ),
          ButtonComponent(
            label: 'Sign in',
            colorButton: HexColor(color_blue_1),
            fontSizeLabel: 18,
            fontWeightLabel: FontWeight.w600,
            loading: loading,
            onPressed: () => submit.call(),
          ),
        ],
      ),
    );
  }

  Future<void> submit() async {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (ctx) => const Setting()),
    );
    // Navigator.of(context).pushAndRemoveUntil(
    //   // push sang màn mới và xóa toàn bộ các stack trước đi
    //   MaterialPageRoute(builder: (ctx) => const Setting()),
    //   (route) => false,
    // );
    // loading = true;
    // setState(() {});
    // print('email: ${email}');
    // print('password: ${password}');
    // await Future.delayed(const Duration(seconds: 2));
    // loading = false;
    // errors['email'] = 'Vui lòng nhập email chuẩn?';
    // setState(() {});
  }
}
